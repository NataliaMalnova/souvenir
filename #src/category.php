<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Категории</title>

    <link rel="shortcut icon" href="/img/fav.png" type="image/png">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

    <?php include 'header.php' ?>
    <div class="setting">

    </div>

    <div class="navBread">
        <div class="container-fluid container center-xs">
            <div class="row">
                <div class="col-xs-12 middle-xs navBread__title">
                   
                        <a href="./">Главная</a>
                        <p>></p>
                        <a href="#">Каталог</a>
                        <p>></p>
                        <a href="#">Сумки</a>
                    
                </div>
            </div>

        </div>
    </div>

    <div class="category__title">
        <div class="container-fluid container category__wrapper"> 
            <div class="row">
                <div class="col-xs-12 column-xs">
                    <h2>Сумки</h2>
                    <div class="category__words">
                        <a href="#">Электроника</a>
                        <a href="#">Спорт и отдых</a>
                        <a href="#">Награды</a>
                        <a href="#">Товары для дома</a>
                        <a href="#">Посуда</a>
                        <a href="#">Деловые подарки</a>
                        <div class="search__selected search__selectedCategory" data-state="">
                            <div class="select__title" data-default="">
                                Ещё 
                            </div>
                            <div class="select__content">
                                <input id="category_0" class="select__input" type="radio" name="category" checked />
                                <label for="category_0" class="select__label">
                                    Ещё
                                </label>
                                <input id="category_1" class="select__input" type="radio" name="category" />
                                <label for="category_1" class="select__label">
                                    1
                                </label>
                                <input id="category_2" class="select__input" type="radio" name="category" />
                                <label for="category_2" class="select__label">
                                    2
                                </label>
                                <input id="category_3" class="select__input" type="radio" name="category" />
                                <label for="category_3" class="select__label">
                                    3
                                </label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <img src="img/categoryTitle.png" alt="сумки" class="categoryTitleImg">
        </div>
    </div>
    <div class="category__content">
        <div class="category__setting-wrapper filter__tovarNone">
            <div class="category__setting">
                <a href="javascript:void(0)">
                    <img src="img/listIcon.svg" alt="">
                    Список
                </a>
                <a href="#header">
                    <img src="img/arrUpBlack.svg" alt="">
                    Наверх
                </a>
                <a href="javascript:void(0)">
                    <img src="img/arrBlack.svg" alt="">
                    Вперёд
                </a>
            </div>
        </div>
        <div class="container-fluid container">
            <div class="row">
                <div class="col-xs-3">
                    <div class="category__filter">
                        <div class="filter__title">
                            <h3>Фильтры</h3>
                            <img src="img/close.svg" alt="" class="closeFilter">
                        </div> 
                        <div class="line__grey"></div>
                        <div class="filter__discont">
                            <div class="checkbox">
                                <input type="checkbox" id="check_discont" class="ch_filter">
                                <label for="check_discont" class="color__red">скидки</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="check_new" class="ch_filter">
                                <label for="check_new" class="color__blue">новинки</label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="check_hit" class="ch_filter">
                                <label for="check_hit" class="color__violet">хиты продаж</label>
                            </div>
                        </div>
                        <div class="filter__specific">
                            <div class="search__selected search__selectedFilter search__selectedPrice" data-state="">
                                <div class="select__title" data-default="">
                                        Цена
                                </div>
                                <div class="select__content">
                                    <input id="filterPrice_0" class="select__input" type="radio" name="filterPrice"  />
                                    <label for="filterPrice_0" class="select__label">
                                        Цена
                                    </label>
                                    <input id="filterPrice_1" class="select__input" type="radio" name="filterPrice" />
                                    <label for="filterPrice_1" class="select__label">
                                        Цена 1
                                    </label>
                                    <input id="filterPrice_2" class="select__input" type="radio" name="filterPrice" />
                                    <label for="filterPrice_2" class="select__label">
                                        Цена 2
                                    </label>
                                    <input id="filterPrice_3" class="select__input" type="radio" name="filterPrice" />
                                    <label for="filterPrice_3" class="select__label">
                                        Цена 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedCount" data-state="">
                                <div class="select__title" data-default="">
                                    Количество
                                </div>
                                <div class="select__content">
                                    <input id="filterCount_0" class="select__input" type="radio" name="filterCount"  />
                                    <label for="filterCount_0" class="select__label">
                                        Количество
                                    </label>
                                    <input id="filterCount_1" class="select__input" type="radio" name="filterCount" />
                                    <label for="filterCount_1" class="select__label">
                                        Количество 1
                                    </label>
                                    <input id="filterCount_2" class="select__input" type="radio" name="filterCount" />
                                    <label for="filterCount_2" class="select__label">
                                        Количество 2
                                    </label>
                                    <input id="filterCount_3" class="select__input" type="radio" name="filterCount" />
                                    <label for="filterCount_3" class="select__label">
                                        Количество 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedColor" data-state="">
                                <div class="select__title" data-default="">
                                    Цвет
                                </div>
                                <div class="select__content">
                                    <input id="filterColor_0" class="select__input" type="radio" name="filterColor"  />
                                    <label for="filterColor_0" class="select__label">
                                        Цвет
                                    </label>
                                    <input id="filterColor_1" class="select__input" type="radio" name="filterColor" />
                                    <label for="filterColor_1" class="select__label">
                                        Цвет 1
                                    </label>
                                    <input id="filterColor_2" class="select__input" type="radio" name="filterColor" />
                                    <label for="filterColor_2" class="select__label">
                                        Цвет 2
                                    </label>
                                    <input id="filterColor_3" class="select__input" type="radio" name="filterColor" />
                                    <label for="filterColor_3" class="select__label">
                                        Цвет 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedBrend" data-state="">
                                <div class="select__title" data-default="">
                                    Бренд
                                </div>
                                <div class="select__content">
                                    <input id="filterBrend_0" class="select__input" type="radio" name="filterBrend"  />
                                    <label for="filterBrend_0" class="select__label">
                                        Бренд
                                    </label>
                                    <input id="filterBrend_1" class="select__input" type="radio" name="filterBrend" />
                                    <label for="filterBrend_1" class="select__label">
                                        Бренд 1
                                    </label>
                                    <input id="filterBrend_2" class="select__input" type="radio" name="filterBrend" />
                                    <label for="filterBrend_2" class="select__label">
                                        Бренд 2
                                    </label>
                                    <input id="filterBrend_3" class="select__input" type="radio" name="filterBrend" />
                                    <label for="filterBrend_3" class="select__label">
                                        Бренд 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedMaterial" data-state="">
                                <div class="select__title" data-default="">
                                    Материалы
                                </div>
                                <div class="select__content">
                                    <input id="filterMaterial_0" class="select__input" type="radio" name="filterMaterial"  checked/>
                                    <label for="filterMaterial_0" class="select__label">
                                        Материалы
                                    </label>
                                    <input id="filterMaterial_1" class="select__input" type="radio" name="filterMaterial" />
                                    <label for="filterMaterial_1" class="select__label">
                                        Материалы 1
                                    </label>
                                    <input id="filterMaterial_2" class="select__input" type="radio" name="filterMaterial" />
                                    <label for="filterMaterial_2" class="select__label">
                                        Материалы 2
                                    </label>
                                    <input id="filterMaterial_3" class="select__input" type="radio" name="filterMaterial" />
                                    <label for="filterMaterial_3" class="select__label">
                                        Материалы 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedMethod" data-state="">
                                <div class="select__title" data-default="">
                                    Метод нанесения
                                </div>
                                <div class="select__content">
                                    <input id="filterMethod_0" class="select__input" type="radio" name="filterMethod" checked/>
                                    <label for="filterMethod_0" class="select__label">
                                        Метод нанесения
                                    </label>
                                    <input id="filterMethod_1" class="select__input" type="radio" name="filterMethod" />
                                    <label for="filterMethod_1" class="select__label">
                                        Метод нанесения 1
                                    </label>
                                    <input id="filterMethod_2" class="select__input" type="radio" name="filterMethod" />
                                    <label for="filterMethod_2" class="select__label">
                                        Метод нанесения 2
                                    </label>
                                    <input id="filterMethod_3" class="select__input" type="radio" name="filterMethod" />
                                    <label for="filterMethod_3" class="select__label">
                                        Метод нанесения 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedSize" data-state="">
                                <div class="select__title" data-default="">
                                    Размеры
                                </div>
                                <div class="select__content">
                                    <input id="filterSize_0" class="select__input" type="radio" name="filterSize" checked/>
                                    <label for="filterSize_0" class="select__label">
                                        Размеры
                                    </label>
                                    <input id="filterSize_1" class="select__input" type="radio" name="filterSize" />
                                    <label for="filterSize_1" class="select__label">
                                        Размеры 1
                                    </label>
                                    <input id="filterSize_2" class="select__input" type="radio" name="filterSize" />
                                    <label for="filterSize_2" class="select__label">
                                        Размеры 2
                                    </label>
                                    <input id="filterSize_3" class="select__input" type="radio" name="filterSize" />
                                    <label for="filterSize_3" class="select__label">
                                        Размеры 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedRusSize" data-state="">
                                <div class="select__title" data-default="">
                                    Российский размер
                                </div>
                                <div class="select__content">
                                    <input id="filterRusSize_0" class="select__input" type="radio" name="filterRusSize" checked/>
                                    <label for="filterRusSize_0" class="select__label">
                                        Российский размер
                                    </label>
                                    <input id="filterRusSize_1" class="select__input" type="radio" name="filterRusSize" />
                                    <label for="filterRusSize_1" class="select__label">
                                        Российский размер 1
                                    </label>
                                    <input id="filterRusSize_2" class="select__input" type="radio" name="filterRusSize" />
                                    <label for="filterRusSize_2" class="select__label">
                                        Российский размер 2
                                    </label>
                                    <input id="filterRusSize_3" class="select__input" type="radio" name="filterRusSize" />
                                    <label for="filterRusSize_3" class="select__label">
                                        Российский размер 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedMechanism" data-state="">
                                <div class="select__title" data-default="">
                                    Вид механизма
                                </div>
                                <div class="select__content">
                                    <input id="filterMechanism_0" class="select__input" type="radio" name="filterMechanism" checked/>
                                    <label for="filterMechanism_0" class="select__label">
                                        Вид механизма
                                    </label>
                                    <input id="filterMechanism_1" class="select__input" type="radio" name="filterMechanism" />
                                    <label for="filterMechanism_1" class="select__label">
                                        Вид механизма 1
                                    </label>
                                    <input id="filterMechanism_2" class="select__input" type="radio" name="filterMechanism" />
                                    <label for="filterMechanism_2" class="select__label">
                                        Вид механизма 2
                                    </label>
                                    <input id="filterMechanism_3" class="select__input" type="radio" name="filterMechanism" />
                                    <label for="filterMechanism_3" class="select__label">
                                        Вид механизмар 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedMaterial2" data-state="">
                                <div class="select__title" data-default="">
                                    Материалы нанесения
                                </div>
                                <div class="select__content">
                                    <input id="filterMaterial2_0" class="select__input" type="radio" name="filterMaterial2" checked/>
                                    <label for="filterMaterial2_0" class="select__label">
                                        Материалы нанесения
                                    </label>
                                    <input id="filterMaterial2_1" class="select__input" type="radio" name="filterMaterial2" />
                                    <label for="filterMaterial2_1" class="select__label">
                                        Материалы нанесения 1
                                    </label>
                                    <input id="filterMaterial2_2" class="select__input" type="radio" name="filterMaterial2" />
                                    <label for="filterMaterial2_2" class="select__label">
                                        Материалы нанесения 2
                                    </label>
                                    <input id="filterMaterial2_3" class="select__input" type="radio" name="filterMaterial2" />
                                    <label for="filterMaterial2_3" class="select__label">
                                        Материалы нанесения 3
                                    </label>
                                </div>
                            </div>

                            <div class="search__selected search__selectedFilter search__selectedMaterial3" data-state="">
                                <div class="select__title" data-default="">
                                    Материалы нанесения
                                </div>
                                <div class="select__content">
                                    <input id="filterMaterial3_0" class="select__input" type="radio" name="filterMaterial3" checked/>
                                    <label for="filterMaterial3_0" class="select__label">
                                        Материалы нанесения
                                    </label>
                                    <input id="filterMaterial3_1" class="select__input" type="radio" name="filterMaterial3" />
                                    <label for="filterMaterial3_1" class="select__label">
                                        Материалы нанесения 1
                                    </label>
                                    <input id="filterMaterial3_2" class="select__input" type="radio" name="filterMaterial3" />
                                    <label for="filterMaterial3_2" class="select__label">
                                        Материалы нанесения 2
                                    </label>
                                    <input id="filterMaterial3_3" class="select__input" type="radio" name="filterMaterial3" />
                                    <label for="filterMaterial3_3" class="select__label">
                                        Материалы нанесения 3
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-9 col-md-12 column-xs filter__tovarNone">
                    <div class="filter__sort-wrapper">
                        <div class="filter__sort">
                            <img src="img/listIcon.svg" alt="" class="openFilter">
                            <h3>Сортировать: </h3>
                            <div class="search__selected search__selectedSort" data-state="">
                                <div class="select__SortTitle">
                                    <div class="select__title" data-default="">
                                        Сначала популярные
                                    </div>
                                    <img src="img/arrGreenOpen.svg" alt="open">
                                </div>
                                <div class="select__content">
                                    <input id="filterSort_0" class="select__input" type="radio" name="filterPrice"  />
                                    <label for="filterSort_0" class="select__label">
                                        Сначала популярные
                                    </label>
                                    <input id="filterSort_1" class="select__input" type="radio" name="filterPrice" />
                                    <label for="filterSort_1" class="select__label">
                                        Сначала популярные 1
                                    </label>
                                    <input id="filterSort_2" class="select__input" type="radio" name="filterPrice" />
                                    <label for="filterSort_2" class="select__label">
                                        Сначала популярные 2
                                    </label>
                                    <input id="filterSort_3" class="select__input" type="radio" name="filterPrice" />
                                    <label for="filterSort_3" class="select__label">
                                        Сначала популярные 3
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="filter__sort filter__sortCount">
                            <p>На странице:</p>
                            <div class="search__selected search__selectedSort search__selectedSort2" data-state="">
                                <div class="select__SortTitle">
                                    <div class="select__title" data-default="30">
                                        30
                                    </div>
                                    <img src="img/arrGreenOpen.svg" alt="open">
                                </div>

                                <div class="select__content">
                                    <input id="filterSort2_0" class="select__input" type="radio" name="filterSort2" value='30' />
                                    <label for="filterSort2_0" class="select__label">
                                        30
                                    </label>
                                    <input id="filterSort2_2" class="select__input" type="radio" name="filterSort2" value='10' />
                                    <label for="filterSort2_2" class="select__label">
                                        10
                                    </label>
                                    <input id="filterSort2_3" class="select__input" type="radio" name="filterSort2" value='3' />
                                    <label for="filterSort2_3" class="select__label">
                                        3
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="line__grey"></div>

                    <div class="category__list">
                        <div class="row">
                            <div class="col-spesh-1">
                                <div class="sliderProduct" data-filterNew>
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorBlue rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_1.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct" data-filter_discont data-filter_new>
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorBlue rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_2.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                    <div class="slederPresent__priceNew">
                                        569,00 ₽
                                    </div>
                                    <div class="slederPresent__priceOld">
                                        669,00 ₽
                                    </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1" >
                                <div class="sliderProduct" data-filter_new>
                                    <div class="sliderProduct__image">
                                        <img src="img/price_3.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct" data-filter_new>
                                    <div class="sliderProduct__image">
                                        <img src="img/price_4.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct" data-filter_hit>
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorBlue rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_5.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <img src="img/price_1.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <img src="img/price_2.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                    <div class="slederPresent__priceNew">
                                        569,00 ₽
                                    </div>
                                    <div class="slederPresent__priceOld">
                                        669,00 ₽
                                    </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorBlue rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_3.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <img src="img/price_4.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorBlue rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_5.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_1.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_2.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                    <div class="slederPresent__priceNew">
                                        569,00 ₽
                                    </div>
                                    <div class="slederPresent__priceOld">
                                        669,00 ₽
                                    </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_3.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_4.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_5.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_1.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_2.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                    <div class="slederPresent__priceNew">
                                        569,00 ₽
                                    </div>
                                    <div class="slederPresent__priceOld">
                                        669,00 ₽
                                    </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_3.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_4.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_5.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_1.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_2.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                    <div class="slederPresent__priceNew">
                                        569,00 ₽
                                    </div>
                                    <div class="slederPresent__priceOld">
                                        669,00 ₽
                                    </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_3.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_4.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_5.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_1.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_2.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                    <div class="slederPresent__priceNew">
                                        569,00 ₽
                                    </div>
                                    <div class="slederPresent__priceOld">
                                        669,00 ₽
                                    </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_3.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_4.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-spesh-1">
                                <div class="sliderProduct">
                                    <div class="sliderProduct__image">
                                        <div class="sliderProduct__discont">
                                            <div class="sliderProduct__discontAfter sliderProduct__colorGreen rowCenter-xs">new</div>
                                            <div class="sliderProduct__discontBefore"></div>
                                        </div>
                                        <img src="img/price_5.png" alt="">
                                    </div>
                                    <div class="sliderProduct__price">
                                        <div class="slederPresent__priceBlack">
                                            569,00 ₽
                                        </div>
                                    </div>
                                    <div class="slederPresent__lineDark"></div>
                                    <h4>Блокнот Daisy, cо страницами для заметок</h4>
                                    <div class="slederPresent__color">
                                        <div class="color__sircle"></div>
                                    </div>
                                    <div class="slederPresent__available">
                                    В наличии – 13895 шт
                                    </div>
                                    <div class="slederPresent__view">
                                        <a href="#" class="slederPresent__viewLink">
                                            Быстрый просмотр
                                        </a>
                                        <a href="#" class="slederPresent__viewBasket">
                                            <img src="img/basketIcon.svg" alt="Корзина">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="category__pagination">
                                <ul>
                                    <li class="paginationActive">1</li>
                                    <li>2</li>
                                    <li>3</li>
                                    <li>4</li>
                                    <li class="paginationNext">Вперёд
                                        <img src="img/arrBlack.svg" alt="">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 column-xs">
                            <p class="pagination__description">
                                Cайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.
                            </p>
                            <div class="openLink popularly__openLink middle-xs">
                                <a href="#" class="plus rowCenter-xs">+</a>
                                <a href="#" class="text">смотреть ещё</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid container filter__tovarNone">
        <div class="row">
            <div class="col-xs-12">
                <div class="line__grey"></div>
            </div>
        </div>
    </div>

    <div class="category__describtion filter__tovarNone">
        <?php include 'footerContainer.php' ?>
    </div>
    <div class="filter__tovarNone">
        <?php include 'footer.php' ?>
    </div>

    <script src='js/script.js'></script>
</body>
</html>