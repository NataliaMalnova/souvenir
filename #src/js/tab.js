const tabs = (tabsSelector, tabSelector, tabContentSelector) => { 
    
    let tab = '';
    let tabContent = '';
    let tabs = '';
    
    if(document.querySelectorAll(tabSelector).length > 0) {
        
        tabs =  document.getElementById(tabsSelector);
        tab = tabs.querySelectorAll(tabSelector);
        tabContent = tabs.querySelectorAll(tabContentSelector);

        hideTabsContent(1);
    }

    tabs.onclick = function (event) {
        
        let target = event.target;
        if (('.' + target.className  == tabSelector) || ('.' + target.parentElement.className == tabSelector)) {
           for (let i = 0; i < tab.length; i++) {
                if (target == tab[i] || target.parentElement == tab[i]) {
                    showTabsContent(i);
                    break;
                }
           }
        }
    }

    function hideTabsContent(a) {
        for (var i=a; i<tabContent.length; i++) {
            tabContent[i].classList.remove('show');
            tabContent[i].classList.add("hide");
            tab[i].classList.remove('whiteborder');
        }
    }
    
    function showTabsContent(b){
        if (tabContent[b].classList.contains('hide')) {
            hideTabsContent(0);
            tab[b].classList.add('whiteborder');
            tabContent[b].classList.remove('hide');
            tabContent[b].classList.add('show');
        }
    }
}
