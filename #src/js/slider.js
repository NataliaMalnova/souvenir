$(document).ready(function() {

  $('.sliderMain').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        //autoplay:true,
        //autoplaySpeed: 2000,
        prevArrow: '<button class="sliderMain__prev"></button>',
        nextArrow: '<button class="sliderMain__next"></button>',
    });

    $('.sliderAll').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        prevArrow: '<button class="sliderAll__prev "></button>',
        nextArrow: '<button class="sliderAll__next"></button>',
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 5
            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 4
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 575,
            settings: {
              slidesToShow: 1
            }
          }
        ]
    });

    $('.sliderBestPrice').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        prevArrow: '<button class="sliderAll__prev"></button>',
        nextArrow: '<button class="sliderAll__next"></button>',
        responsive: [
            {
              breakpoint: 1738,
              settings: {
                slidesToShow: 5
              }
            },
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 575,
              settings: {
                slidesToShow: 1
              }
            }
          ]
    });

    $('.sliderNewProdact').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        prevArrow: '<button class="sliderAll__prev"></button>',
        nextArrow: '<button class="sliderAll__next"></button>',
        responsive: [
            {
              breakpoint: 1738,
              settings: {
                slidesToShow: 5
              }
            },
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 575,
              settings: {
                slidesToShow: 1
              }
            }
          ]
    });

    $('.sliderCompany').slick({
      infinite: true,
      slidesToShow: 7,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1000,
      prevArrow: '<button class="sliderCompany__control sliderCompany__prev "></button>',
      nextArrow: '<button class="sliderCompany__control sliderCompany__next"></button>',
      responsive: [
          {
            breakpoint: 1738,
            settings: {
              slidesToShow: 6
            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 4
            }
          },
          {
            breakpoint: 575,
            settings: {
              slidesToShow: 2
            }
          }
        ]
  });
  
});   