const showOptions = (optionsClick, tabShow) => {  

    const options = document.querySelectorAll(optionsClick);
    const tab = document.querySelectorAll(tabShow);
    

    options.forEach((item, i) => {
        item.addEventListener('click', () => {

            options.forEach(k => {
                k.classList.remove('card__optionActive');
        
            });
            item.classList.add('card__optionActive');
            tab.forEach((item, i) => {
                item.style.display = 'none';
            });

            tab[i].style.display = 'flex';
        });

    });
}
