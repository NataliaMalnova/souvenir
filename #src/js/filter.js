/**
 * 
 *   data атрибуты для фильтров: 
 *      * data-filter_discont - СКИДКИ
 *      * data-filter_new - НОВИНКИ
 *      * data-filter_hit - ХИТЫ ПРОДАЖ
 *      
 */

const filter = () => {
    
    try{
        const ch_all = document.querySelectorAll('.ch_filter');
        const ch_discont = document.getElementById('check_discont');
        const ch_new = document.getElementById('check_new');
        const ch_hit = document.getElementById('check_hit');
        const productList  = document.querySelectorAll('.sliderProduct');

        const sortCount  = document.querySelectorAll('.search__selectedSort2 input');

        ch_all.forEach(it => {
            it.addEventListener('click', () => {
                
                productList.forEach(item => {
                    item.parentNode.style.display = 'none';
                });
                if(ch_discont.checked) {
                    productList.forEach(item => {
                        if(item.hasAttribute('data-filter_discont') ){
                            item.parentNode.style.display = 'flex';
                        }  
                    });     
                }
                if(ch_new.checked) {
                    productList.forEach(item => {
                        if(item.hasAttribute('data-filter_new') ){
                            item.parentNode.style.display = 'flex';
                        }  
                    });     
                }
                if(ch_hit.checked) {
                    productList.forEach(item => {
                        if(item.hasAttribute('data-filter_hit') ){
                            item.parentNode.style.display = 'flex';
                        }  
                    });     
                }
                if(!ch_discont.checked && !ch_new.checked && !ch_hit.checked){
                    productList.forEach(item => {
                        item.parentNode.style.display = 'flex'; 
                    }); 
                }
            });
        });

        sortCount.forEach(item => {
            item.addEventListener('click', () => {
                
                if(item.value < productList.length + 1){
                    productList.forEach(it => {
                        it.parentNode.style.display = 'none';
                    });

                    for(let i = 0; i < item.value; i++){
                        productList[i].parentNode.style.display = 'flex';
                    }
                }
            });
        });
        
    } catch(e) {}

}
