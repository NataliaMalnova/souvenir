
//  ЦИФРЫ И ЗАПЯТЫЕ

const inputValidNamAndComm = (inputNumber) => {  

    const num = document.querySelectorAll(inputNumber);

    num.forEach(item => {
		item.addEventListener('input', (e) => {
            item.value = item.value.replace(/[^\d,]/g, '');

		});
    });
    
};

//  ТОЛЬКО ЦИФРЫ

const inputValidNum = (inputNumber) => {  
    
    const num = document.querySelectorAll(inputNumber);
    num.forEach(item => {
		item.addEventListener('input', (e) => {
            item.value = item.value.replace (/\D/g, '');
		});
    });
    
};

//  ВВОД ДО ОПРЕДЕЛЕННОГО ЗНАЧЕНИЯ

const inputValidValue = (inputNumber, maxValue) => {  
    
    const num = document.querySelectorAll(inputNumber);
    const numMax = maxValue;

    num.forEach(item => {
		item.addEventListener('input', (e) => {
            item.value = item.value.replace (/\D/g, '');
            if (item.value > numMax) {
                item.value = numMax;
            } 
		});
    });
    
};