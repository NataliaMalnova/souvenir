const drop = (fileId) => {
    
    try{
        const fileInputs = document.getElementById(fileId);

        fileInputs.addEventListener('input', () => {
    
            let dots;
    
            const arr = fileInputs.files[0].name.split('.');
        
            arr[0].length > 9 ? dots = '...' : dots = '.';
            const name = arr[0].substring(0, 9) + dots + arr[1];
            //console.log(document.querySelector('label[for="'+ fileId +'"]'));
            document.querySelector('label[for="' + fileId + '"]>span.file__text').textContent = name;
            document.querySelector('label[for="' + fileId + '"]>img').src='img/file_after.svg';

        });

        document.querySelector('label[for="' + fileId).addEventListener('mouseover', () => {
            if(document.querySelector('label[for="' + fileId + '"]>span.file__text').textContent == 'Загрузить макет') {
                document.querySelector('label[for="' + fileId + '"]>img').src='img/file_hover.svg';
            }
        });

        document.querySelector('label[for="' + fileId).addEventListener('mouseout', () => {
            if(document.querySelector('label[for="' + fileId + '"]>span.file__text').textContent == 'Загрузить макет') {
                document.querySelector('label[for="' + fileId + '"]>img').src='img/file_before.svg';
            }
        });

    } catch(e) {}
}