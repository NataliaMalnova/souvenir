const burger = (menuelector, burgerSelector) => {

    const menuElem = document.querySelector(menuelector),
		  burgerElem = document.querySelector(burgerSelector);
		  
    const menuElemList = menuElem.querySelectorAll('a');
    const menuClose = menuElem.querySelectorAll('.menuMobileClose');

    menuElem.classList.remove('menuMobileOpen');

    burgerElem.addEventListener('click', () => {
		//if(menuElem.style.display == 'none' && window.screen.availWidth < 993)
		if(!menuElem.classList.contains('menuMobileOpen') && window.innerWidth < 993) {
			menuElem.classList.add('menuMobileOpen');
			document.body.style.overflowY = "hidden";
            document.body.style.height = "100%";
        } else {
			menuElem.classList.remove('menuMobileOpen');
			document.body.style.overflowY = "auto";
            //document.body.style.height = "auto";
        }
	});
	
	menuElemList.forEach(item => {
		item.addEventListener('click', () => {
			if(menuElem.classList.contains('menuMobileOpen')) {
				menuElem.classList.remove('menuMobileOpen');
				document.body.style.overflowY = "auto";
			}
		});
    });
    
    menuClose.forEach(item => {
		item.addEventListener('click', () => {
			if(menuElem.classList.contains('menuMobileOpen')) {
				menuElem.classList.remove('menuMobileOpen');
				document.body.style.overflowY = "auto";
			}
		});
	});


    window.addEventListener('resize', () => {
        if(window.innerWidth > 992) {
            menuElem.style.display = 'none';
        }
	});
	
}

try{
	const sortOpen = document.querySelector('.openFilter');
	const sortClose = document.querySelector('.closeFilter');
	const sort = document.querySelector('.category__filter');
	
	function showFilter(display) {
		document.querySelectorAll('.filter__tovarNone').forEach(item => {
			item.style.display = display;
		});
	}
	sortOpen.addEventListener('click', () => {
		sort.classList.add('filterSortOpen');	
		setTimeout(showFilter, 1000, 'none');
	});
	
	sortClose.addEventListener('click', () => {
		sort.classList.remove('filterSortOpen');
		setTimeout(showFilter, 30, 'block');
	}); 
} catch(e) {}



const showDemands = (click, closeSelector, overlaySelector) => {

	const demands = document.querySelector(click);
	const close = document.querySelector(closeSelector);
	const overlay = document.querySelector(overlaySelector);

    demands.addEventListener('click', function(event) {
		event.preventDefault();
		document.querySelector('.overlay_demands').style.display = 'flex';
		document.body.style.overflowY = 'hidden';
	});

	close.addEventListener('click', function(event) {
		event.preventDefault();
		document.querySelector('.overlay_demands').style.display = 'none';

		document.body.style.overflowY = 'visible';
	});

	overlay.addEventListener('click', function(event) {
		//event.preventDefault();
		if (event.target != this) { return true; }
			document.querySelector('.overlay_demands').style.display = 'none';
			document.body.style.overflowY = 'visible';
	});
	
}
