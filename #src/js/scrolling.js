const scrolling = () => {

    let links = document.querySelectorAll('[href^="#"]'),
        speed = 0.3;

    links.forEach(link => {
        link.addEventListener('click', function(event){
            event.preventDefault();
            let widthTop, hash, toBlock, start;
                
            if(link.classList.contains('demands_links')){
                widthTop = document.querySelector('.demands__text').scrollTop - 100;
                hash = this.hash;
                toBlock = document.querySelector(hash).getBoundingClientRect().top;
                start = null;
                
                requestAnimationFrame(step);

            } else {
                widthTop = document.documentElement.scrollTop;
                hash = this.hash;
                toBlock = document.querySelector(hash).getBoundingClientRect().top;
                start = null;
                requestAnimationFrame(step);
            }
            
            function step(time) {
                if(start === null) {
                    start = time;
                }

                let progress = time - start,
                r = (toBlock < 0 ? Math.max(widthTop - progress/speed, widthTop + toBlock) : Math.min(widthTop + progress/speed, widthTop + toBlock)) ;
                    
                console.log('r = ', r);
                if(link.classList.contains('demands_links')){
                    document.querySelector('.demands__text').scrollTo(0, r);

                } else{
                    document.documentElement.scrollTo(0, r);
                }
                
                if(r != widthTop + toBlock) {
                    requestAnimationFrame(step);
                } else {
                    //location.hash = hash;
                }
            }
        });
    });
};