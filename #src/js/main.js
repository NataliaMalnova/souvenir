document.addEventListener("DOMContentLoaded", () => {

	//burger('.menuMobile', '.burger');
	
	/** 
	 * 
	 * функция для плавного скролла к якорной ссылке. 
	 * Срабатывается автоматически на все ссылки с адресом #	
	 * 
	**/

	scrolling();

	/**
	 * 
	 * функция для переключения табов. 
	 * Параматры: 
	 * 		tabs - id контейнера с табами,
	 * 		.tab__item - селектор класса с заголовком для определенного таба,
	 * 		.tabContent - селектор класса с контетом для определенного таба
	 * 
	 */
	
	tabs('tabs', '.tab__item', '.tabContent');
	tabs('tabs2', '.tab__item', '.tabContent');
	tabs('tabs3', '.tab__item', '.tabContent');
	tabs('tabs4', '.tab__item', '.tabContent');
	tabs('tabs5', '.tab__item', '.tabContent');
	tabs('tab_prodCharact', '.tab__item', '.tabContent');

	/**
	 * 
	 * функция для переключения вида товара на странице карточки товара
	 * Параматры;
	 * 		card__option - селектор класса товара, на который нажали
	 * 		tab_product - селектор класса табов, которые нужно показать
	 * 
	 */

	showOptions('.card__option', '.tab_product');

	selectOpen('.search__selected1');
	selectOpen('.search__selected2');
	selectOpen('.search__selected3');
	selectOpen('.search__selected4');
	selectOpen('.search__selected5');
	selectOpen('.search__selected6');

	selectOpen('.search__selectedCategory');
	selectOpenNotDelete('.search__selectedPrice');
	selectOpenNotDelete('.search__selectedCount');
	selectOpenNotDelete('.search__selectedColor');
	selectOpenNotDelete('.search__selectedBrend');
	selectOpenNotDelete('.search__selectedMaterial');
	selectOpenNotDelete('.search__selectedMethod');
	selectOpenNotDelete('.search__selectedSize');
	selectOpenNotDelete('.search__selectedRusSize');
	selectOpenNotDelete('.search__selectedMechanism');
	selectOpenNotDelete('.search__selectedMaterial2');
	selectOpenNotDelete('.search__selectedMaterial3');
	selectOpenNotDelete('.search__selectedSort');
	selectOpenNotDelete('.search__selectedSort2');

	/**
	 * 
	 * функция для показа дополнительных характеристик товара.
	 * Параматры:
	 * 		.add__app - селектор класса кнопки, для показа
	 * 		.card_form - селектор класса формы с доп.характеристиками
	 * 		.card__formFile - селектор класса для появление загрузки файла
	 * 		.formClose - селектор класса кнопки закрытия формы
	 *      .card__calcResult - селектор класса поля с калькулятором результатов для доп.параметров
	 * 
	 */

	openAdditions('.add__app', '.card_form', '.card__formFile', '.formClose', '.card__calcResult');

	/**
	 * 
	 * функция для замены надписи при загрузке файла 
	 * на название загруженного файла
	 */

	drop('file__1');
	drop('file__2');
	drop('file__3');

	/**
	 * 
	 * функция для запрета ввода числа до определенного символа
	 */

	inputValidValue('#paramHeight', 70);
	inputValidValue('#paramHeight2', 70);
	inputValidValue('#paramHeight3', 70);

	inputValidValue('#paramWidth', 50);
	inputValidValue('#paramWidth2', 50);
	inputValidValue('#paramWidth3', 50);

	/**
	 * 
	 * функция для ввода только чисел
	 */

	inputValidNum('.input__quantity');

	/**
	 * 
	 * функция для фильтров на странице категории
	 */
	
	filter();

	showDemands('#demands_1', '.demands_close', '.overlay_demands .container');
	showDemands('#demands_2', '.demands_close', '.overlay_demands .container');
	showDemands('#demands_3', '.demands_close', '.overlay_demands .container');

	document.onclick = function(e){

		if ( event.target.className != 'burger' ) {
			if(document.querySelector('.menuMobile').classList.contains('menuMobileOpen')) {
				document.querySelector('.menuMobile').classList.remove('menuMobileOpen');
			}
		};
	};
});