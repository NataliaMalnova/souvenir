<footer>
    <div class="container-fluid container">
      <div class="footer__phone">
        <p>Бесплатный звонок по всей России</p>
        <a href="#">
          <img src="img/phoneIcon.svg" alt="">
        8 921 755 21 33
        </a>
      </div>
      <div class="footer__line"></div>
      <div class="row top-xs" >
        <div class="col-xs-5 col-sm-6">
          <div class="footer__contact">
            <p>Санкт-Петербург, ул. Красная 14</p>
            <a href="#">info@suvenir.ru</a>
            <div class="footer__sochial">
              <a href="#">
                <img src="img/vk.svg" alt="vk">
              </a>
              <a href="#">
                <img src="img/face.svg" alt="face">
              </a>
              <a href="#">
                <img src="img/whatsapp.svg" alt="whatsapp">
              </a>
              <a href="#">
                <img src="img/telegram.svg" alt="telegram">
              </a>
            </div>
          </div>
        </div>
        <div class="col-xs-4 col-sm-6">
          <div class="menu__details">
            <ul>
              <li class="menu__detailsTitle">
                Подробнее
              </li>
              <li>
                <a href="#">О компании</a>
              </li>
              <li>
                <a href="#">Услуги</a>
              </li>
              <li>
                <a href="#">Акции</a>
              </li>
              <li>
                <a href="#">Контакты</a>
              </li>
            </ul>
          </div>
          <div class="menu__help">
            <ul>
              <li class="menu__detailsTitle">
                Помощь
              </li>
              <li>
                <a href="#">Как сделать заказ</a>
              </li>
              <li>
                <a href="#">Доставка</a>
              </li>
              <li>
                <a href="#">Оплата</a>
              </li>
              <li>
                <a href="#">Требования к макетам</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-xs-3 col-sm-12">
          <div class="footer__download">
            <img src="img/file.svg" alt="download">
            <p>Скачать требования <br> к макетам для печати</p>
          </div>
        </div>
        <div class="col-xs-12 between-xs">
          <div class="footer__politics">
            <p>Сувениры.рф © Все права защищены, 2019 г.</p>
            <div class="footer__politics-link">
              <a href="#">Политика обработки данных</a> /
              <a href="#"> Политика конфиденциальности</a>
            </div>
          </div>
          <div class="footer__up">
            <a href="#header">
              <img src="img/arrWhite.svg" alt="up">
              Наверх
            </a>
          </div>
        </div>
      </div>
    </div>
  </footer>