<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Карточка товара</title>
    <link rel="shortcut icon" href="/img/fav.png" type="image/png">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php include 'header.php' ?>

    <div class="navBread">
        <div class="container-fluid container">
            <div class="navBread__title middle-xs">
                <a href="./">Главная</a>
                <p>></p>
                <a href="#">Каталог</a>
                <p>></p>
                <a href="#">Сумки</a>
                <p>></p>
                <a href="#">Сумка Fellow</a>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="container-fluid container"> 
            <div class="row">
                <div class="col-xs-6 col-smm-12">
                    <div id="tabs" class="tab_all tab_product">
                        <div class="tabs__title">
                            <div class="tab__item whiteborder">
                                <img src="img/bag-yellowSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-yellowSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-yellowSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-yellowSmall.png" alt="сумка">
                            </div>
                        </div>
                        <div class="tabs__content">
                            <div class="tabContent">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/bag-yellowBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/bag-yellowBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabs2" class="tab_all tab_product">
                        <div class="tabs__title">
                            <div class="tab__item whiteborder">
                                <img src="img/bag-redSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-redSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-redSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-redSmall.png" alt="сумка">
                            </div>
                        </div>
                        <div class="tabs__content">
                            <div class="tabContent">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/bag-yellowBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabs3" class="tab_all tab_product">
                        <div class="tabs__title">
                            <div class="tab__item whiteborder">
                                <img src="img/bag-whiteSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-whiteSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-whiteSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-whiteSmall.png" alt="сумка">
                            </div>
                        </div>
                        <div class="tabs__content">
                            <div class="tabContent">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/bag-yellowBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/bag-yellowBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabs4" class="tab_all tab_product">
                        <div class="tabs__title">
                            <div class="tab__item whiteborder">
                                <img src="img/bag-blackSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-blackSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-blackSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-blackSmall.png" alt="сумка">
                            </div>
                        </div>
                        <div class="tabs__content">
                            <div class="tabContent">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/bag-yellowBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabs5" class="tab_all tab_product">
                        <div class="tabs__title">
                            <div class="tab__item whiteborder">
                                <img src="img/bag-blueSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-blueSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-blueSmall.png" alt="сумка">
                            </div>
                            <div class="tab__item">
                                <img src="img/bag-blueSmall.png" alt="сумка">
                            </div>
                        </div>
                        <div class="tabs__content">
                            <div class="tabContent">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/bag-redSmall.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/bag-yellowBig.png" alt="image">
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="card__imageBig">
                                    <div class="sliderProduct__discont">
                                        <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                        <div class="sliderProduct__discontBefore"></div>
                                    </div>
                                    <img src="img/shirt-blackBig.png" alt="image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-smm-12">
                    <div class="card__character">
                        <h1>Сумка для покупок Fellow, зеленое яблоко с ярко-синим</h1>
                        <div class="card__code middle-xs between-xs">
                            <p>Артикул – 11941205</p>
                            <a href="#">
                                Поделиться
                                <img src="img/share.svg" alt="Поделиться">
                            </a>
                        </div>
                        <div class="line__grey"></div>
                        <div class="price middle-xs card__price">
                            <p class="price__new">569,00 ₽</p>
                            <p class="price__old">669,00 ₽
                            </p>
                        </div>
                        <div class="line__grey"></div>
                        <div class="card__change">
                            <p>Выберите цвет:</p>
                            <div class="card__changeOption">
                                <div class="card__option card__optionActive">
                                    <img src="img/bag-yellowSmall.png" alt="1">
                                </div>
                                <div class="card__option">
                                    <img src="img/bag-redSmall.png" alt="1">
                                </div>
                                <div class="card__option">
                                    <img src="img/bag-whiteSmall.png" alt="1">
                                </div>
                                <div class="card__option">
                                    <img src="img/bag-blackSmall.png" alt="1">
                                </div>
                                <div class="card__option">
                                    <img src="img/bag-blueSmall.png" alt="1">
                                </div>
                            </div>
                            <form action="#" class="card_form card_form1">
                                <div class="card_formTitle">
                                    <h4>Лицевая сторона</h4>
                                    <img src="img/close.svg" alt="close" class="formClose">
                                </div>
                                <div class="card_formAnswer">
                                    <div class="search__selected search__selected1" data-state="">
                                        <div class="select__title" data-default="">
                                            Тип нанесения
                                        </div>
                                        <div class="select__content">
                                            <input id="sel1_0" class="select__input" type="radio" name="sel1" checked />
                                            <label for="sel1_0" class="select__label">
                                                Тип нанесения
                                            </label>
                                            <input id="sel1_1" class="select__input" type="radio" name="sel1" />
                                            <label for="sel1_1" class="select__label">
                                                Трафаретная печать
                                            </label>
                                            <input id="sel1_2" class="select__input" type="radio" name="sel1" />
                                            <label for="sel1_2" class="select__label">
                                                Трафаретная печать 2
                                            </label>
                                            <input id="sel1_3" class="select__input" type="radio" name="sel1" />
                                            <label for="sel1_3" class="select__label">
                                                Трафаретная печать 3
                                            </label>
                                        </div>
                                    </div>
                                    <div class="search__selected search__selected2" data-state="">
                                        <div class="select__title" data-default="">
                                            Выберите цвета
                                        </div>
                                        <div class="select__content">
                                            <input id="sel2_0" class="select__input" type="radio" name="sel2" checked />
                                            <label for="sel2_0" class="select__label">
                                                Выберите цвета
                                            </label>
                                            <input id="sel2_1" class="select__input" type="radio" name="sel2" />
                                            <label for="sel2_1" class="select__label">
                                                4 цвета
                                            </label>
                                            <input id="sel2_2" class="select__input" type="radio" name="sel2" />
                                            <label for="sel2_2" class="select__label">
                                                3 цвета
                                            </label>
                                            <input id="sel2_3" class="select__input" type="radio" name="sel2" />
                                            <label for="sel2_3" class="select__label">
                                                2 цвета
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card__formPar">
                                    <div class="card__formParam">
                                        <input 
                                            type = "text" 
                                            id = "paramHeight" 
                                            class = "form__param"
                                            placeholder = 'Высота, мм'
                                        >
                                        <p>X</p>
                                        <input 
                                            type="text" 
                                            id="paramWidth" 
                                            class="form__param"
                                            placeholder='Высота, мм'
                                        >
                                        
                                    </div>
                                    <p class="card__formParamComent">max 70 x 50 mm</p>
                                    </div>
      
                                    
                                </div>
                            </form>
                            <div class="card__formFile">
                                <div class="file">
                                    <input type="file" name="file" id="file__1" class="file__input">
                                    <label for="file__1" class="file__label">
                                        <img class="file__img" src="img/file_before.svg" alt="Прикрепите файл">
                                    <span class="file__text">Загрузить макет</span>
                                    </label>
                                </div>
                                <a href="" id="demands_1">Требования к макетам</a>
                            </div>

                            <form action="#" class="card_form card_form2">
                                <div class="card_formTitle">
                                    <h4>Обратная сторона</h4>
                                    <img src="img/close.svg" alt="close" class="formClose">
                                </div>
                                <div class="card_formAnswer">
                                    <div class="search__selected search__selected3" data-state="">
                                        <div class="select__title" data-default="">
                                            Тип нанесения
                                        </div>
                                        <div class="select__content">
                                            <input id="sel3_0" class="select__input" type="radio" name="sel3" checked />
                                            <label for="sel3_0" class="select__label">
                                                Тип нанесения
                                            </label>
                                            <input id="sel3_1" class="select__input" type="radio" name="sel3" />
                                            <label for="sel3_1" class="select__label">
                                                Трафаретная печать
                                            </label>
                                            <input id="sel3_2" class="select__input" type="radio" name="sel3" />
                                            <label for="sel3_2" class="select__label">
                                                Трафаретная печать 2
                                            </label>
                                            <input id="sel3_3" class="select__input" type="radio" name="sel3" />
                                            <label for="sel3_3" class="select__label">
                                                Трафаретная печать 3
                                            </label>
                                        </div>
                                    </div>
                                    <div class="search__selected search__selected4" data-state="">
                                        <div class="select__title" data-default="">
                                            Выберите цвета
                                        </div>
                                        <div class="select__content">
                                            <input id="sel4_0" class="select__input" type="radio" name="sel4" checked />
                                            <label for="sel4_0" class="select__label">
                                                Выберите цвета
                                            </label>
                                            <input id="sel4_1" class="select__input" type="radio" name="sel4" />
                                            <label for="sel4_1" class="select__label">
                                                4 цвета
                                            </label>
                                            <input id="sel4_2" class="select__input" type="radio" name="sel4" />
                                            <label for="sel4_2" class="select__label">
                                                3 цвета
                                            </label>
                                            <input id="sel4_3" class="select__input" type="radio" name="sel4" />
                                            <label for="sel4_3" class="select__label">
                                                2 цвета
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card__formPar">
                                    <div class="card__formParam">
                                        <input 
                                            type = "text" 
                                            id = "paramHeight2" 
                                            class = "form__param"
                                            placeholder = 'Высота, мм'
                                        >
                                        <p>X</p>
                                        <input 
                                            type="text" 
                                            id="paramWidth2" 
                                            class="form__param"
                                            placeholder='Высота, мм'
                                        >
                                        
                                    </div>
                                    <p class="card__formParamComent">max 70 x 50 mm</p>
                                    </div>
      
                                    
                                </div>
                            </form>
                            <div class="card__formFile">
                                <div class="file">
                                    <input type="file" name="file" id="file__2" class="file__input">
                                    <label for="file__2" class="file__label">
                                        <img class="file__img" src="img/file_before.svg" alt="Прикрепите файл">
                                    <span class="file__text">Загрузить макет</span>
                                    </label>
                                </div>
                                <a href="" id="demands_2">Требования к макетам</a>
                            </div>

                            <form action="#" class="card_form card_form3">
                                <div class="card_formTitle">
                                    <h4>Ручки</h4>
                                    <img src="img/close.svg" alt="close" class="formClose">
                                </div>
                                <div class="card_formAnswer">
                                    <div class="search__selected search__selected5" data-state="">
                                        <div class="select__title" data-default="">
                                            Тип нанесения
                                        </div>
                                        <div class="select__content">
                                            <input id="sel5_0" class="select__input" type="radio" name="sel5" checked />
                                            <label for="sel5_0" class="select__label">
                                                Тип нанесения
                                            </label>
                                            <input id="sel5_1" class="select__input" type="radio" name="sel5" />
                                            <label for="sel5_1" class="select__label">
                                                Трафаретная печать
                                            </label>
                                            <input id="sel5_2" class="select__input" type="radio" name="sel5" />
                                            <label for="sel5_2" class="select__label">
                                                Трафаретная печать 2
                                            </label>
                                            <input id="sel5_3" class="select__input" type="radio" name="sel5" />
                                            <label for="sel5_3" class="select__label">
                                                Трафаретная печать 3
                                            </label>
                                        </div>
                                    </div>
                                    <div class="search__selected search__selected6" data-state="">
                                        <div class="select__title" data-default="">
                                            Выберите цвета
                                        </div>
                                        <div class="select__content">
                                            <input id="sel6_0" class="select__input" type="radio" name="sel6" checked />
                                            <label for="sel6_0" class="select__label">
                                                Выберите цвета
                                            </label>
                                            <input id="sel6_1" class="select__input" type="radio" name="sel6" />
                                            <label for="sel6_1" class="select__label">
                                                4 цвета
                                            </label>
                                            <input id="sel6_2" class="select__input" type="radio" name="sel6" />
                                            <label for="sel6_2" class="select__label">
                                                3 цвета
                                            </label>
                                            <input id="sel6_3" class="select__input" type="radio" name="sel6" />
                                            <label for="sel6_3" class="select__label">
                                                2 цвета
                                            </label>
                                        </div>
                                    </div>
                                    <div class="card__formPar">
                                    <div class="card__formParam">
                                        <input 
                                            type = "text" 
                                            id = "paramHeight3" 
                                            class = "form__param"
                                            placeholder = 'Высота, мм'
                                        >
                                        <p>X</p>
                                        <input 
                                            type="text" 
                                            id="paramWidth3" 
                                            class="form__param"
                                            placeholder='Высота, мм'
                                        >
                                        
                                    </div>
                                    <p class="card__formParamComent">max 70 x 50 mm</p>
                                    </div>
      
                                    
                                </div>
                            </form>
                            <div class="card__formFile">
                                <div class="file">
                                    <input type="file" name="file" id="file__3" class="file__input">
                                    <label for="file__3" class="file__label">
                                        <img class="file__img" src="img/file_before.svg" alt="Прикрепите файл">
                                    <span class="file__text">Загрузить макет</span>
                                    </label>
                                </div>
                                <a href="" id="demands_3">Требования к макетам</a>
                            </div>

                            <button class="add__app">
                                <img src="img/plusIcon.svg" alt="Добавить нанесение">
                                Добавить нанесение
                            </button>
                        </div>
                        <div class="line__grey"></div>
                        <div class="card__quantity">
                            <div class="quantity">
                                <p>Количество</p>
                                <input 
                                    type="text"
                                    placeholder = '13800 шт'
                                    class="input__quantity"
                                >
                                <img src="img/lockIcon.svg" alt="">
                            </div>
                            <div class="free">
                                <p>Свободно</p>
                                <p class="freeCount">13800 шт</p>
                            </div>
                        </div>
                        <div class="card__calcResult">
                            <div class="line__grey"></div>
                            <div class="row card__calcTabl">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4 end-xs">
                                    <p>1 шт</p>
                                </div>
                                <div class="col-xs-4 end-xs">
                                    <p>125 штук</p>
                                </div>
                            </div>
                            <div class="row card__calcTablRow">
                                <div class="col-xs-4">
                                    <p>Товар</p>
                                </div>
                                <div class="col-xs-4 end-xs">
                                    <p class="carcPrice">569<span>,00</span></p>
                                </div>
                                <div class="col-xs-4 end-xs">
                                <p class="carcPrice">71 125<span>,00</span></p>
                                </div>
                            </div>
                            <div class="row card__calcTablRow">
                                <div class="col-xs-4">
                                    <p>Нанесение</p>
                                </div>
                                <div class="col-xs-4 end-xs">
                                    <p class="carcPrice">217<span>,00</span></p>
                                </div>
                                <div class="col-xs-4 end-xs">
                                <p class="carcPrice">27 125<span>,00</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="line__dark"></div>
                        <div class="card__result">
                            <div class="card__resultPrice">
                                <p>Итого</p>
                                <p class="resultPrice">569,00  ₽</p>
                            </div>
                            <div class="card__resultTerm">
                                Готовность заказа <span>&ensp;2 дня</span> 
                                <img src="img/questionIcon.svg" alt="Готовность заказа">
                            </div>
                        </div>
                        <button class="card__add">Добавить в корзину</button>
                    </div>
                </div>
                <div class="col-xs-12 column-xs">
                    <div class="line__grey"></div>
                    <p class="card__description">
                    Легкая и удобная сумка из натурального хлопка идеально подходит для похода по магазинам или прогулок по городу. Удобные ручки высотой 34 см, прочные, специально обработанные швы, а также большая площадь под нанесение делают ее отличным и недорогим инструментом для продвижения. Сумка доступна во множестве цветов , поэтому каждый клиент может подобрать подходящую цветовую гамму.
                    </p>
                    <div class="line__grey"></div>
                    <div id="tab_prodCharact" class="tab_all">
                        <div class="tabs__title">
                            <div class="tab__item whiteborder">
                            Характеристики
                            </div>
                            <div class="tab__item">
                            Упаковка
                            </div>
                        </div>
                        <div class="line__grey"></div>
                        <div class="tabs__content">
                            <div class="tabContent ">
                                <div class="prodCharact__text">
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Вид нанесения</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        
                                        <p class="prodCharact__value">
                                        Трафаретная печать, Текстильный принтер, Вышивка, Термотрансфер, Трафаретная печать + вытравка, Трафаретная печать + светотражающая краска, Трафаретная печать + фольга
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Вес</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                            300 г
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Бренд</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        US Basic
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Цвет</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        Лайм
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Маетриал товара</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        Нетканый полипропилен, хлопок
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Плотность</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        80 г/м2
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Длина ручек</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        34 см
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Размер товара</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        40 х 38 см
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="tabContent hide">
                                <div class="prodCharact__text">
                                    <h4>Индивидуальная упаковка</h4>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Вид упквки</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        Чехол
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Вес упаковки</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                            300 г
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Описание упаковки</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        Чехол на кнопках из ЭВА в цвет изделия
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Размер упаковки</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        290 x 220 x 5 мм
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Объем единицы</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        320 см³
                                        </p>
                                    </div>
                                </div>
                                <div class="prodCharact__text">
                                    <h4>Транспортная упковка</h4>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Вид упквки</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        Транспортнаая упаковка
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Вес упаковки</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                            15 500 г
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Колличество в упаковке</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        50 шт
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Размер упаковки</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        500 x 400 x 400 мм
                                        </p>
                                    </div>
                                    <div class="prodCharact__line">
                                        <div class="prodCharact__name">
                                            <p>Объем единицы</p>
                                            <div class="line_dotted"></div>
                                        </div>
                                        <p class="prodCharact__value">
                                        0.08 м³
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card__words">
                        <p>Спорт и отдых</p>
                        <p>Награды</p>
                        <p>Товары для дома</p>
                        <p>Товары для дома</p>
                    </div>
                    <div class="line__grey"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="slider_similar">
        <div class="container-fluid container">
            <div class="row">
                <div class="col-xs-12">
                <h2>похожие товары</h2>
                </div>
            </div>
            <div class="col-xs-12 column-xs">
                <div class="sliderBestPrice sliderSimilar sliderAll__wrapper">
                    <div class="sliderProduct__item">
                        <div class="sliderProduct">
                            <div class="sliderProduct__image">
                            <div class="sliderProduct__discont">
                                <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                <div class="sliderProduct__discontBefore"></div>
                            </div>
                            <img src="img/price_1.png" alt="">
                            </div>
                            <div class="sliderProduct__price">
                            <div class="slederPresent__priceBlack">
                                569,00 ₽
                            </div>
                            </div>
                            <div class="slederPresent__lineDark"></div>
                            <h4>Блокнот Daisy, cо страницами для заметок</h4>
                            <div class="slederPresent__color">
                            <div class="color__sircle"></div>
                            </div>
                            <div class="slederPresent__available">
                            В наличии – 13895 шт
                            </div>
                            <div class="slederPresent__view">
                            <a href="#" class="slederPresent__viewLink">
                                Быстрый просмотр
                            </a>
                            <a href="#" class="slederPresent__viewBasket">
                                <img src="img/basketIcon.svg" alt="Корзина">
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="sliderProduct__item ">
                        <div class="sliderProduct">
                            <div class="sliderProduct__image">
                            <div class="sliderProduct__discont">
                                <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                                <div class="sliderProduct__discontBefore"></div>
                            </div>
                            <img src="img/price_2.png" alt="">
                            </div>
                            <div class="sliderProduct__price">
                            <div class="slederPresent__priceNew">
                                569,00 ₽
                            </div>
                            <div class="slederPresent__priceOld">
                                669,00 ₽
                            </div>
                            </div>
                            <div class="slederPresent__lineDark"></div>
                            <h4>Блокнот Daisy, cо страницами для заметок</h4>
                            <div class="slederPresent__color"></div>
                            <div class="slederPresent__available">
                            В наличии – 13895 шт
                            </div>
                            <div class="slederPresent__view">
                            <a href="#" class="slederPresent__viewLink">
                                Быстрый просмотр
                            </a>
                            <a href="#" class="slederPresent__viewBasket">
                                <img src="img/basketIcon.svg" alt="Корзина">
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="sliderProduct__item ">
                        <div class="sliderProduct">
                            <div class="sliderProduct__image">
                            <img src="img/price_3.png" alt="">
                            </div>
                            <div class="sliderProduct__price">
                            <div class="slederPresent__priceBlack">
                                569,00 ₽
                            </div>
                            </div>
                            <div class="slederPresent__lineDark"></div>
                            <h4 class="greenColor">Блокнот Daisy, cо страницами для заметок</h4>
                            <div class="slederPresent__color">
                            <div class="color__sircle">
                                <div class="color__sircleItem black"></div>
                                <div class="color__sircleItem color__sircleItemActive grey"></div>
                                <div class="color__sircleItem red"></div>
                                <div class="color__sircleItem brown"></div>
                                <div class="color__sircleItem green"></div>
                                <div class="color__sircleItem yellow"></div>
                                <a href="#" class="colorOpen"></a>
                            </div>
                            </div>
                            <div class="slederPresent__available">
                            В наличии – 13895 шт
                            </div>
                            <div class="slederPresent__view">
                            <a href="#" class="slederPresent__viewLink">
                                Быстрый просмотр
                            </a>
                            <a href="#" class="slederPresent__viewBasket">
                                <img src="img/basketIcon.svg" alt="Корзина">
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="sliderProduct__item ">
                        <div class="sliderProduct">
                            <div class="sliderProduct__image">
                       
                            <img src="img/price_4.png" alt="">
                            </div>
                            <div class="sliderProduct__price">
                            <div class="slederPresent__priceBlack">
                                569,00 ₽
                            </div>
                            </div>
                            <div class="slederPresent__lineDark"></div>
                            <h4>Блокнот Daisy, cо страницами для заметок</h4>
                            <div class="slederPresent__color"></div>
                            <div class="slederPresent__available">
                            В наличии – 13895 шт
                            </div>
                            <div class="slederPresent__view">
                            <a href="#" class="slederPresent__viewLink">
                                Быстрый просмотр
                            </a>
                            <a href="#" class="slederPresent__viewBasket">
                                <img src="img/basketIcon.svg" alt="Корзина">
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_5.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4>Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color">
                        <div class="color__sircle">
                            <div class="color__sircleItem color__sircleItemActive blue"></div>
                            <div class="color__sircleItem greenLight"></div>
                            <div class="color__sircleItem red"></div>
                            <div class="color__sircleItem FFDA16"></div>
                        </div>
                        </div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_6.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4>Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color"></div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_1.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4>Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color">
                        <div class="color__sircle">
                            <div class="color__sircleItem black"></div>
                            <div class="color__sircleItem color__sircleItemActive grey"></div>
                            <div class="color__sircleItem red"></div>
                            <div class="color__sircleItem brown"></div>
                            <div class="color__sircleItem green"></div>
                            <div class="color__sircleItem gold"></div>
                            <a href="#" class="colorOpen"></a>
                        </div>
                        </div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_2.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4>Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color"></div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_3.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4 class="greenColor">Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color">
                        <div class="color__sircle">
                            <div class="color__sircleItem black"></div>
                            <div class="color__sircleItem color__sircleItemActive grey"></div>
                            <div class="color__sircleItem red"></div>
                            <div class="color__sircleItem brown"></div>
                            <div class="color__sircleItem green"></div>
                            <div class="color__sircleItem yellow"></div>
                            <a href="#" class="colorOpen"></a>
                        </div>
                        </div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="line__grey"></div>
    <div class="slider_similar">
        <div class="container-fluid container">
            <div class="row">
                <div class="col-xs-12">
                <h2>вы смотрели</h2>
                </div>
            </div>
            <div class="col-xs-12 column-xs">
                <div class="sliderBestPrice sliderSimilar sliderAll__wrapper">
                    <div class="sliderProduct__item">
                        <div class="sliderProduct">
                            <div class="sliderProduct__image">
                            <div class="sliderProduct__discont">
                                <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                                <div class="sliderProduct__discontBefore"></div>
                            </div>
                            <img src="img/price_9.png" alt="">
                            </div>
                            <div class="sliderProduct__price">
                            <div class="slederPresent__priceBlack">
                                569,00 ₽
                            </div>
                            </div>
                            <div class="slederPresent__lineDark"></div>
                            <h4>Блокнот Daisy, cо страницами для заметок</h4>
                            <div class="slederPresent__color">
                            <div class="color__sircle"></div>
                            </div>
                            <div class="slederPresent__available">
                            В наличии – 13895 шт
                            </div>
                            <div class="slederPresent__view">
                            <a href="#" class="slederPresent__viewLink">
                                Быстрый просмотр
                            </a>
                            <a href="#" class="slederPresent__viewBasket">
                                <img src="img/basketIcon.svg" alt="Корзина">
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="sliderProduct__item ">
                        <div class="sliderProduct">
                            <div class="sliderProduct__image">
                            <div class="sliderProduct__discont">
                                <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                                <div class="sliderProduct__discontBefore"></div>
                            </div>
                            <img src="img/price_4.png" alt="">
                            </div>
                            <div class="sliderProduct__price">
                            <div class="slederPresent__priceNew">
                                569,00 ₽
                            </div>
                            <div class="slederPresent__priceOld">
                                669,00 ₽
                            </div>
                            </div>
                            <div class="slederPresent__lineDark"></div>
                            <h4>Блокнот Daisy, cо страницами для заметок</h4>
                            <div class="slederPresent__color"></div>
                            <div class="slederPresent__available">
                            В наличии – 13895 шт
                            </div>
                            <div class="slederPresent__view">
                            <a href="#" class="slederPresent__viewLink">
                                Быстрый просмотр
                            </a>
                            <a href="#" class="slederPresent__viewBasket">
                                <img src="img/basketIcon.svg" alt="Корзина">
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="sliderProduct__item ">
                        <div class="sliderProduct">
                            <div class="sliderProduct__image">
                            <img src="img/price_2.png" alt="">
                            </div>
                            <div class="sliderProduct__price">
                            <div class="slederPresent__priceBlack">
                                569,00 ₽
                            </div>
                            </div>
                            <div class="slederPresent__lineDark"></div>
                            <h4 class="greenColor">Блокнот Daisy, cо страницами для заметок</h4>
                            <div class="slederPresent__color">
                            <div class="color__sircle">
                                <div class="color__sircleItem black"></div>
                                <div class="color__sircleItem color__sircleItemActive grey"></div>
                                <div class="color__sircleItem red"></div>
                                <div class="color__sircleItem brown"></div>
                                <div class="color__sircleItem green"></div>
                                <div class="color__sircleItem yellow"></div>
                                <a href="#" class="colorOpen"></a>
                            </div>
                            </div>
                            <div class="slederPresent__available">
                            В наличии – 13895 шт
                            </div>
                            <div class="slederPresent__view">
                            <a href="#" class="slederPresent__viewLink">
                                Быстрый просмотр
                            </a>
                            <a href="#" class="slederPresent__viewBasket">
                                <img src="img/basketIcon.svg" alt="Корзина">
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="sliderProduct__item ">
                        <div class="sliderProduct">
                            <div class="sliderProduct__image">
                       
                            <img src="img/price_10.png" alt="">
                            </div>
                            <div class="sliderProduct__price">
                            <div class="slederPresent__priceBlack">
                                569,00 ₽
                            </div>
                            </div>
                            <div class="slederPresent__lineDark"></div>
                            <h4>Блокнот Daisy, cо страницами для заметок</h4>
                            <div class="slederPresent__color"></div>
                            <div class="slederPresent__available">
                            В наличии – 13895 шт
                            </div>
                            <div class="slederPresent__view">
                            <a href="#" class="slederPresent__viewLink">
                                Быстрый просмотр
                            </a>
                            <a href="#" class="slederPresent__viewBasket">
                                <img src="img/basketIcon.svg" alt="Корзина">
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs sliderProduct__colorBlue">new</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_12.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4>Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color">
                        <div class="color__sircle">
                            <div class="color__sircleItem color__sircleItemActive blue"></div>
                            <div class="color__sircleItem greenLight"></div>
                            <div class="color__sircleItem red"></div>
                            <div class="color__sircleItem FFDA16"></div>
                        </div>
                        </div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_6.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4>Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color"></div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_1.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4>Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color">
                        <div class="color__sircle">
                            <div class="color__sircleItem black"></div>
                            <div class="color__sircleItem color__sircleItemActive grey"></div>
                            <div class="color__sircleItem red"></div>
                            <div class="color__sircleItem brown"></div>
                            <div class="color__sircleItem green"></div>
                            <div class="color__sircleItem gold"></div>
                            <a href="#" class="colorOpen"></a>
                        </div>
                        </div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_2.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4>Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color"></div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                    <div class="sliderProduct__item ">
                    <div class="sliderProduct">
                        <div class="sliderProduct__image">
                        <div class="sliderProduct__discont">
                            <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                            <div class="sliderProduct__discontBefore"></div>
                        </div>
                        <img src="img/price_3.png" alt="">
                        </div>
                        <div class="sliderProduct__price">
                        <div class="slederPresent__priceNew">
                            569,00 ₽
                        </div>
                        <div class="slederPresent__priceOld">
                            669,00 ₽
                        </div>
                        </div>
                        <div class="slederPresent__lineDark"></div>
                        <h4 class="greenColor">Блокнот Daisy, cо страницами для заметок</h4>
                        <div class="slederPresent__color">
                        <div class="color__sircle">
                            <div class="color__sircleItem black"></div>
                            <div class="color__sircleItem color__sircleItemActive grey"></div>
                            <div class="color__sircleItem red"></div>
                            <div class="color__sircleItem brown"></div>
                            <div class="color__sircleItem green"></div>
                            <div class="color__sircleItem yellow"></div>
                            <a href="#" class="colorOpen"></a>
                        </div>
                        </div>
                        <div class="slederPresent__available">
                        В наличии – 13895 шт
                        </div>
                        <div class="slederPresent__view">
                        <a href="#" class="slederPresent__viewLink">
                            Быстрый просмотр
                        </a>
                        <a href="#" class="slederPresent__viewBasket">
                            <img src="img/basketIcon.svg" alt="Корзина">
                        </a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="line__grey"></div>
    <div class="card__container">
        <?php include 'footerContainer.php' ?>
    </div>
    <?php include 'overlayDemands.php' ?>
    <?php include 'footer.php' ?>

    <script src='js/script.js'></script>
</body>
</html>