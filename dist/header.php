<header id="header">
    <ul class="menuMobile">
      <img src="img/close.png" alt="" class="menuMobileClose">
      <li><a href="#">Готовые решения</a></li>
      <li><a href="#">Акции</a></li>
      <li><a href="#">Требования к макетам</a>
      </li>
      <li><a href="#">Доставка</a></li>
      <li><a href="#">Оплата</a></li>
      
      <li><a href="#">Контакты</a></li>
    </ul>
    <div class="container-fluid container">
        <div class="row between-xs">
          <div class="col-xs-8 col-sm-4 start-xs">
            <!-- <img src="img/menuOpen.svg" alt="menu__open" class="menu__open"> -->
            <img src="img/menuOpen.svg" alt="menu" class="burger">
            
            <nav class="header__menu">
              <ul>
                <li>
                  <a href="#">Готовые решения</a>
                  <div class="line-hover"></div>
                </li>
                <li><a href="#">Акции</a></li>
                <li><a href="#">Требования к макетам</a></li>
                <li><a href="#">Доставка</a></li>
                <li><a href="#">Оплата</a></li>
                <li><a href="#">Контакты</a></li>
              </ul>
            </nav>
          </div>
          <div class="col-xs-4 col-sm-8 end-xs middle-xs header__linkRow column-xss bottom-xss">
            <a href="#" class="header__link  middle-xs ">
              <img src="img/mesIcon.svg" alt="email" class="mssage">
              chenada@suvenir.ru
            </a>

            <a href="#" class="header__link  middle-xs">
              <img src="img/mapIcon.svg" alt="map" class="map">
              Санкт-Петербург
              <img src="img/triangleIcon.svg" alt="open" class="open">
            </a>
          </div>
          <div class="col-xs-12 ">
            <div class="header__search middle-xs between-xs bottom-md column-xss ">
              <div class="header__formRow row-xs">
                <a href="index.php">
                  <img src="img/logo.svg" alt="logo" class="header__logo">
                </a>
                
                <form class="header__searchForm" action="#">
                  <button class="header__catalog middle-xs center-xs">
                    <img src="img/catalogIcon.svg" alt="Каталог">
                    <p>Каталог</p>
                  </button>
                  <input type="text" placeholder="Хочу найти..." name="search">
                  <button type="submit">
                    <img src="img/searchIcon.svg" alt="поиск">
                  </button>
                </form>
              </div>

              <div class="header__phoneButton row-xs column-md bottom-md row-xss between-xss">
                <a href="#" class="header__phone middle-xs">
                  +7 (921) 755 21 33
                  <img src="img/triangleIcon.svg" alt="phone">
                </a>
                <button class="header__call middle-xs center-xs">
                  Заказать звонок
                </button>
                <div class="header__basket middle-xs between-xs">
                  <div class="basket__icon center-xs">
                    <img src="img/basketIcon.svg" alt="корзина">
                  </div>
                  <button class="basket__btn">
                    <div class="busket__count center-xs middle-xs ">10</div>
                    Корзина
                  </button>
                </div>
              </div>

            </div>
          </div>
        </div>
    </div>
  </header>