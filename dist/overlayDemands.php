<div class="overlay_demands">
    <div class="container-fluid container">
        <div class="demands">
            <div class="demands__header">
                <p>Требования к макетам для печати</p>
                <div class="demands__headerLink">
                    <a href="#">
                        <img src="img/downloadIconGrey.svg" alt="Скачать файлом">
                        Скачать файлом
                    </a>
                    <img src="img/close.svg" alt="close" class="demands_close">
                </div>
            </div>
            <div class="line__grey"></div>
            <div class="demands__content">
                <div class="row">
                    <div class="col-xs-4 between-xs">
                        <div class="demands__link">
                            <a href="#generalRequirements" class="demands_links demands__linkActive">Общие требования</a>
                            <a href="#generalRequirements2" class="demands_links">Шелкография</a>
                            <a href="#generalRequirements3"
                            class="demands_links">Флекс</a>
                            <a href="#generalRequirements4" class="demands_links">Вышивка</a>
                            <a href="#generalRequirements5" class="demands_links">Тампопечать</a>
                            <a href="#generalRequirements6" class="demands_links">УФ-печать</a>
                            <a href="#generalRequirements7" class="demands_links">Надглазурная деколь</a>
                            <a href="#generalRequirements8" class="demands_links">Лазерная гравировка</a>
                            <a href="#generalRequirements9" class="demands_links">Тиснение</a>
                            <a href="#" class="demands_links">Сублимационная печать</a>
                            <a href="#" class="demands_links">Объемная наклейка/a>
                            <a href="#" class="demands_links">Цифровая печать</a>
                            <a href="#" class="demands_links">Прямая печать на ткани</a>
                            <a href="#" class="demands_links">Вязание с логотипом</a>
                            <a href="#" class="demands_links">Печать на футболках</a>
                        </div>
                        <div class="line__horizontal"></div>
                    </div>
                    <div class="col-xs-8">
                        <div class="demands__text">
                            <h3 id="generalRequirements">Общие требования к макетам</h3>
                            <p>
                                Чтобы ваш логотип на сувенирах был напечатан правильно, нужно правильно подготовить оригинал-макеты. Каждый вид нанесения имеет свои технологические особенности и ограничения. С ними лучше ознакомиться заранее. Если макеты не соответствуют техническим требованиям, они в лучшем случае потребуют переделки, а в худшем — приведут к браку печати.
                            </p>
                            <div class="line__grey">&nbsp;</div>
                            <ol>
                                <li>
                                    В макете не должно быть эффектов (градиент, прозрачность, тень, линза и т.п.), невидимых или заблокированных слоев/объектов.
                                </li>
                                <div class="line__grey">&nbsp;</div>
                                <li>
                                    Шрифты необходимо перевести в кривые.
                                </li>
                                <li>
                                    Все растровые изображения должны быть выполнены в масштабе 1:1
                                с разрешением не менее 300 dpi. Допустимые форматы файлов для изображений – tiff, psd. Важную информацию необходимо размещать не ближе 2 мм к линии реза.
                                </li>
                                <div class="line__grey">&nbsp;</div>
                                <li>
                                    <p>
                                    Предоставляйте оригинал-макеты в формате PDF.
                                    <a href="#">Как сохранить PDF файлы, выполненные в программе CorelDRAW?</a>
                                    </p>
                                </li>
                                <div class="line__grey">&nbsp;</div>
                            </ol>
                            <h3 id="generalRequirements2">
                                Шёлкография</h3>
                            <div class="row">
                                <div class="col-xs-4 column-xs">
                                    <p class="demand__title">Цвет нанесения:</p>
                                    <p class="demand__describtion">
                                    Pantone C*
                                    CMYK (полноцвет)*
                                    </p>
                                </div>
                                <div class="col-xs-8 column-xs">
                                    <p class="demand__title">
                                        Параметры:
                                    </p>
                                    <p class="demand__describtion">
                                        Минимальная толщина линий 0,5 мм без учета подложки и 0,6 мм с подложкой, инверсных линий 0,7 мм; минимальная плотность растра 20 %
                                    </p>
                                </div>
                            </div>
                            <div class="line__grey">&nbsp;</div>
                            <h3 id="generalRequirements3">
                            Флекс</h3>
                            <div class="row">
                                <div class="col-xs-4 column-xs">
                                    <p class="demand__title">
                                        Цвет нанесения:
                                    </p>
                                    <p class="demand__describtion">
                                        по каталогу <br>
                                        (только 1 цвет)
                                    </p>
                                </div>
                                <div class="col-xs-8 column-xs">
                                    <p class="demand__title">
                                        Параметры:
                                    </p>
                                    <p class="demand__describtion">
                                        Мин. толщ. линий 1 мм, пробельных линий 1 мм; минимальная длина элемента 3 мм
                                    </p>
                                </div>
                            </div>
                            <div class="line__grey">&nbsp;</div>

                            <h3 id="generalRequirements4">
                                Вышивка
                            </h3>
                            <div class="row">
                                <div class="col-xs-4 column-xs">
                                    <p class="demand__title">
                                        Цвет нанесения:
                                    </p>
                                    <p class="demand__describtion">
                                        Pantone C*
                                    </p>
                                </div>
                                <div class="col-xs-8 column-xs">
                                    <p class="demand__title">
                                        Параметры:
                                    </p>
                                    <p class="demand__describtion">
                                        Минимальная толщина линий 1 мм, высота букв 5 мм
                                    </p>
                                </div>
                            </div>
                            <div class="line__grey">&nbsp;</div>

                            <h3 id="generalRequirements5">
                                Тампопечать
                            </h3>
                            <div class="row">
                                <div class="col-xs-4 column-xs">
                                    <p class="demand__title">
                                        Цвет нанесения:
                                    </p>
                                    <p class="demand__describtion">
                                    Pantone C* <br>
                                    Pantone C**
                                    </p>
                                </div>
                                <div class="col-xs-8 column-xs">
                                    <p class="demand__title">
                                        Параметры:
                                    </p>
                                    <p class="demand__describtion">
                                        минимальная толщина печатных и пробельных линий 0,15 мм
                                    </p>
                                </div>
                            </div>
                            <div class="line__grey">&nbsp;</div>

                            <h3 id="generalRequirements6">
                                УФ-печать
                            </h3>
                            <div class="row">
                                <div class="col-xs-4 column-xs">
                                    <p class="demand__title">
                                        Цвет нанесения:
                                    </p>
                                    <p class="demand__describtion">
                                        CMYK + White <br>
                                        (полноцвет)*
                                    </p>
                                </div>
                                <div class="col-xs-8 column-xs">
                                    <p class="demand__title">
                                        Параметры:
                                    </p>
                                    <p class="demand__describtion">
                                        мин. толщ. линий 0,15 мм; охранное поле и вылет 2 мм; разрешение растрового изображения 300–600 dpi
                                    </p>
                                </div>
                            </div>
                            <div class="line__grey">&nbsp;</div>

                            <h3 id="generalRequirements7">
                                Надглазурная деколь
                            </h3>
                            <div class="row">
                                <div class="col-xs-4 column-xs">
                                    <p class="demand__title">
                                        Цвет нанесения:
                                    </p>
                                    <p class="demand__describtion">
                                        Heraeus* <br>
                                        CMYK (полноцвет)*
                                    </p>
                                </div>
                                <div class="col-xs-8 column-xs">
                                    <p class="demand__title">
                                        Параметры:
                                    </p>
                                    <p class="demand__describtion">
                                        минимальная толщина линий 0,4 мм, инверсных линий 0,6 мм; минимальная плотность растра 20 %
                                    </p>
                                </div>
                            </div>
                            <div class="line__grey">&nbsp;</div>

                            <h3 id="generalRequirements8">
                                Лазерная гравировка
                            </h3>
                            <div class="row">
                                <div class="col-xs-4 column-xs">
                                    <p class="demand__title">
                                        Цвет нанесения:
                                    </p>
                                    <p class="demand__describtion">
                                        зависит от материала изделия
                                    </p>
                                </div>
                                <div class="col-xs-8 column-xs">
                                    <p class="demand__title">
                                        Параметры гравировки по металлу:
                                    </p>
                                    <p class="demand__describtion">
                                        Минимальная толщина горизонтальных линий 0,12 мм, вертикальных линий 0,15 мм, инверсных линий 0,25 мм; расстояние между соседними линиями 0,3 мм
                                    </p>
                                    <p class="demand__title">
                                    Параметры гравировки по стеклу, дереву, коже
                                    и искусственной коже, пластику (в т.ч. софт-тач), окрашенным и лакированным поверхностям:
                                    </p>
                                    <p class="demand__describtion">
                                        Минимальная толщина линий 0,3 мм, инверсных линий 0,4 мм; высота букв не менее 2 мм
                                    </p>
                                    <p class="demand__title">
                                        Параметры гравировки по флису:
                                    </p>
                                    <p class="demand__describtion">
                                        Минимальная толщина линий 1 мм, высота букв 5 мм
                                    </p>
                                </div>
                            </div>
                            <div class="line__grey">&nbsp;</div>
                            <h3 id="generalRequirements9">
                                Как сохранить PDF файлы в программе CorelDRAW
                            </h3>
                            <p>
                                С января 2016 года мы принимаем в работу оригинал-макеты исключительно в формате PDF. Макеты, созданные в программе CorelDRAW, можно корректно сохранить в виде пригодных для печати PDF-файлов. <br><br>

                                В этом вам поможет несложная инструкция:
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>