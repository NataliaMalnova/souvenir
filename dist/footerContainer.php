
<div class="container-fluid container">
    <div class="row ">
        <div class="col-xs-3 col-smm-6">
            <div class="statistic__wrapper">
                <div class="statistic__wrapper-cont statistic__wrapper-cont1"></div>
                <p class="statistic__wrapper-text">
                    Доставка по всей России точно в срок, а иногда
                    и быстрее
                </p>
            </div>
        </div>
        <div class="col-xs-3 col-smm-6">
            <div class="statistic__wrapper">
                <div class="statistic__wrapper-cont statistic__wrapper-cont2"></div>
                <p class="statistic__wrapper-text">
                    Большой выбор сувениров из России, Европы и Азии
                </p>
            </div>
        </div>
        <div class="col-xs-3 col-smm-6">
            <div class="statistic__wrapper">
                <div class="statistic__wrapper-cont statistic__wrapper-cont3"></div>
                <p class="statistic__wrapper-text">
                    Наши картинки не стираются. К каждому товару дадим инструкцию по использованию.
                </p>
            </div>
        </div>
        <div class="col-xs-3 col-smm-6">
            <div class="statistic__wrapper">
                <div class="statistic__wrapper-cont statistic__wrapper-cont4"></div>
                <p class="statistic__wrapper-text">
                    Сделем или скорректируем макет для печати
                    при необходимости
                </p>
            </div>
        </div>
    </div>
</div>