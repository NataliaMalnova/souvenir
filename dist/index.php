<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Сувенир</title>
  
  <link rel="shortcut icon" href="/img/fav.png" type="image/png">
  <link rel="stylesheet" href="css/style.css">

</head>
<body>

  <?php include 'header.php' ?>
  
  <div class="main">
    <div class="container-fluid container">
      <div class="row">
        <div class="col-xs-9 col-sm-12">
          <div class="sliderMain single-item slideMain_wrapper">
            <div class="slider__item slider__item1">
              <div class="sliderMain__content">
                <h3>
                  Сумка-шоппер, <br> экологичный и удобный тренд
                </h3>
                <p>Подарок, который пригодится каждому</p>
                <a href="#">Выбрать сумки</a>
              </div>
            </div>
            <div class="slider__item slider__item1">
              <div class="sliderMain__content">
                <h3>
                  Сумка-шоппер, <br> экологичный и удобный тренд
                </h3>
                <p>Подарок, который пригодится каждому</p>
                <a href="#">Выбрать сумки</a>
              </div>
            </div>
            <div class="slider__item slider__item1">
              <div class="sliderMain__content">
                <h3>
                  Сумка-шоппер, <br> экологичный и удобный тренд
                </h3>
                <p>Подарок, который пригодится каждому</p>
                <a href="#">Выбрать сумки</a>
              </div>
            </div>
            <div class="slider__item slider__item1">
              <div class="sliderMain__content">
                <h3>
                  Сумка-шоппер, <br> экологичный и удобный тренд
                </h3>
                <p>Подарок, который пригодится каждому</p>
                <a href="#">Выбрать сумки</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-3 col-sm-5 col-smm-7 col-xss-12">
          <div class="stock">
            <div class="stock__title between-xs">
              <h3>акция недели</h3>
              <div class="stock__discount">
                <div class="stock__discountBefore middle-xs center-xs">-50%</div>
                <div class="stock__discountHover"></div>
              </div>
            </div>
            <p class="stock__describtion">Блокнот Daisy, cо страницами для заметок</p>
            <div class="stock__img center-xs">
              <img src="img/stock__1.png" alt="Блокнот Daisy, cо страницами для заметок">
            </div>
            <div class="price middle-xs">
              <p class="price__new">569,00 ₽</p>
              <p class="price__old">669,00 ₽
              </p>
            </div>
            <div class="price__star">
              <img src="img/star.svg" alt="1">
              <img src="img/star.svg" alt="2">
              <img src="img/star.svg" alt="3">
              <img src="img/star.svg" alt="4">
              <img src="img/star.svg" alt="5">
            </div>
            <p class="stoke__available">В наличии – 13895 шт</p>
            <a href="#" class="stock__link"><img src="img/arrWhite.svg" alt="link"></a>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="line"></div>
        </div>

      </div>
    </div>
  </div>
  <div class="popularly">
    <div class="container-fluid container">
      <div class="row">
        <div class="col-xs-12">
          <h3>популярно сейчас</h3>
        </div>
        <div class="col-xs-12 column-xs">
          <div class="sliderAll sliderAll__wrapper">
              <div class="sliderAll__item">
                <div class="slederPresent">
                  <div class="slederPresent__image">
                    <img src="img/populary_1.png" alt="Ручки">
                  </div>
                  <h4>Ручки</h4>
                  <div class="slederPresent__lineDark"></div>
                  <div class="slederPresent__price between-xs middle-xs">
                    <p>от 690, 00 ₽</p>
                    <a href="#" class="slederPresent__link center-xs">
                      <img src="img/arrWhite.svg" alt="link"> 
                    </a>
                  </div>
                  <div class="slederPresent__line"></div>
                </div>
              </div>
              <div class="sliderAll__item ">
                <div class="slederPresent">
                  <div class="slederPresent__image">
                    <img src="img/populary_2.png" alt="Подарочные наборы">
                  </div>
                  <h4>Подарочные наборы</h4>
                  <div class="slederPresent__lineDark"></div>
                  <div class="slederPresent__price between-xs middle-xs">
                    <p>от 690, 00 ₽</p>
                    <a href="#" class="slederPresent__link center-xs">
                      <img src="img/arrWhite.svg" alt="link"> 
                    </a>
                  </div>
                  <div class="slederPresent__line"></div>
                </div>
              </div>
              <div class="sliderAll__item ">
                <div class="slederPresent">
                  <div class="slederPresent__image">
                    <img src="img/populary_3.png" alt="Подарочные наборы">
                  </div>
                  <h4>Подарочные наборы</h4>
                  <div class="slederPresent__lineDark"></div>
                  <div class="slederPresent__price between-xs middle-xs">
                    <p>от 690, 00 ₽</p>
                    <a href="#" class="slederPresent__link center-xs">
                      <img src="img/arrWhite.svg" alt="link"> 
                    </a>
                  </div>
                  <div class="slederPresent__line"></div>
                </div>
              </div>
              <div class="sliderAll__item ">
                <div class="slederPresent">
                  <div class="slederPresent__image">
                    <img src="img/populary_4.png" alt="Подарочные наборы">
                  </div>
                  <h4>Подарочные наборы</h4>
                  <div class="slederPresent__lineDark"></div>
                  <div class="slederPresent__price between-xs middle-xs">
                    <p>от 690, 00 ₽</p>
                    <a href="#" class="slederPresent__link center-xs">
                      <img src="img/arrWhite.svg" alt="link"> 
                    </a>
                  </div>
                  <div class="slederPresent__line"></div>
                </div>
              </div>
              <div class="sliderAll__item ">
                <div class="slederPresent">
                  <div class="slederPresent__image">
                    <img src="img/populary_5.png" alt="Подарочные наборы">
                  </div>
                  <h4>Подарочные наборы</h4>
                  <div class="slederPresent__lineDark"></div>
                  <div class="slederPresent__price between-xs middle-xs">
                    <p>от 690, 00 ₽</p>
                    <a href="#" class="slederPresent__link center-xs">
                      <img src="img/arrWhite.svg" alt="link"> 
                    </a>
                  </div>
                  <div class="slederPresent__line"></div>
                </div>
              </div>
              <div class="sliderAll__item ">
                <div class="slederPresent">
                  <div class="slederPresent__image">
                    <img src="img/populary_6.png" alt="Зонты">
                  </div>
                  <h4>Зонты</h4>
                  <div class="slederPresent__lineDark"></div>
                  <div class="slederPresent__price between-xs middle-xs">
                    <p>от 690, 00 ₽</p>
                    <a href="#" class="slederPresent__link center-xs">
                      <img src="img/arrWhite.svg" alt="link"> 
                    </a>
                  </div>
                  <div class="slederPresent__line"></div>
                </div>
              </div>
              <div class="sliderAll__item ">
                <div class="slederPresent">
                  <div class="slederPresent__image">
                    <img src="img/populary_3.png" alt="Зонты">
                  </div>
                  <h4>Подарочные наборы</h4>
                  <div class="slederPresent__lineDark"></div>
                  <div class="slederPresent__price between-xs middle-xs">
                    <p>от 690, 00 ₽</p>
                    <a href="#" class="slederPresent__link center-xs">
                      <img src="img/arrWhite.svg" alt="link"> 
                    </a>
                  </div>
                  <div class="slederPresent__line"></div>
                </div>
              </div>
              <div class="sliderAll__item ">
                <div class="slederPresent">
                  <div class="slederPresent__image">
                    <img src="img/populary_4.png" alt="Зонты">
                  </div>
                  <h4>Подарочные наборы</h4>
                  <div class="slederPresent__lineDark"></div>
                  <div class="slederPresent__price between-xs middle-xs">
                    <p>от 690, 00 ₽</p>
                    <a href="#" class="slederPresent__link center-xs">
                      <img src="img/arrWhite.svg" alt="link"> 
                    </a>
                  </div>
                  <div class="slederPresent__line"></div>
                </div>
              </div>
          </div>
          <div class="openLink popularly__openLink middle-xs">
            <a href="#" class="plus rowCenter-xs">+</a>
            <a href="#" class="text">смотреть ещё</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="categories">
    <div class="container-fluid container">
      <div class="row">
        <div class="col-xs-12">
          <div class="line"></div>
        </div>
      </div>
      <div class="row categories__list">
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item1">
              <h4>Офисная канцелярия</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item2">
              <h4 class="blackColor">Одежда</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item3">
              <h4>Новогодние подарки</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item4">
              <h4>Аксессуары</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item5">
              <h4>Электроника</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item6">
              <h4>Автомобильные аксессуары</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item7">
              <h4 class="blackColor">Дом и интерьер</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item8">
              <h4 class="blackColor">Съедобные подарки</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item9">
              <h4 class="blackColor">Подарки для детей</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item10">
              <h4 class="blackColor">Спорт и отдых</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item11">
              <h4 >Призы и награды</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item12">
              <h4>Подарочные наборы</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item13">
              <h4 class="blackColor">Сувениры на заказ</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item14">
              <h4>Промо</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item15">
              <h4>Праздники</h4>
              <a href="#"></a>
          </div>
        </div>
        <div class="col-xs-3 col-smm-6">
          <div class="categories__item categories__item16">
              <h4>Упаковка</h4>
              <a href="#"></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bestPrice">
    <div class="container-fluid container">
    <div class="line"></div>
      <div class="row">
        <div class="col-xs-12">
          <h2>лучшая цена</h2>
        </div>
      </div>
      <div class="col-xs-12 column-xs">
        <div class="sliderBestPrice sliderAll__wrapper">
            <div class="sliderProduct__item">
              <div class="sliderProduct">
                <div class="sliderProduct__image">
                  <div class="sliderProduct__discont">
                    <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                    <div class="sliderProduct__discontBefore"></div>
                  </div>
                  <img src="img/price_1.png" alt="">
                </div>
                <div class="sliderProduct__price">
                  <div class="slederPresent__priceNew">
                    569,00 ₽
                  </div>
                  <div class="slederPresent__priceOld">
                    669,00 ₽
                  </div>
                </div>
                <div class="slederPresent__lineDark"></div>
                <h4>Блокнот Daisy, cо страницами для заметок</h4>
                <div class="slederPresent__color">
                  <div class="color__sircle">
                      <div class="color__sircleItem black"></div>
                      <div class="color__sircleItem color__sircleItemActive grey"></div>
                      <div class="color__sircleItem red"></div>
                      <div class="color__sircleItem brown"></div>
                      <div class="color__sircleItem green"></div>
                      <div class="color__sircleItem gold"></div>
                      <a href="#" class="colorOpen"></a>
                  </div>
                </div>
                <div class="slederPresent__available">
                  В наличии – 13895 шт
                </div>
                <div class="slederPresent__view">
                  <a href="#" class="slederPresent__viewLink">
                    Быстрый просмотр
                  </a>
                  <a href="#" class="slederPresent__viewBasket">
                    <img src="img/basketIcon.svg" alt="Корзина">
                  </a>
                </div>
              </div>
            </div>
            <div class="sliderProduct__item ">
              <div class="sliderProduct">
                <div class="sliderProduct__image">
                  <div class="sliderProduct__discont">
                    <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                    <div class="sliderProduct__discontBefore"></div>
                  </div>
                  <img src="img/price_2.png" alt="">
                </div>
                <div class="sliderProduct__price">
                  <div class="slederPresent__priceNew">
                    569,00 ₽
                  </div>
                  <div class="slederPresent__priceOld">
                    669,00 ₽
                  </div>
                </div>
                <div class="slederPresent__lineDark"></div>
                <h4>Блокнот Daisy, cо страницами для заметок</h4>
                <div class="slederPresent__color"></div>
                <div class="slederPresent__available">
                  В наличии – 13895 шт
                </div>
                <div class="slederPresent__view">
                  <a href="#" class="slederPresent__viewLink">
                    Быстрый просмотр
                  </a>
                  <a href="#" class="slederPresent__viewBasket">
                    <img src="img/basketIcon.svg" alt="Корзина">
                  </a>
                </div>
              </div>
            </div>
            <div class="sliderProduct__item ">
              <div class="sliderProduct">
                <div class="sliderProduct__image">
                  <div class="sliderProduct__discont">
                    <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                    <div class="sliderProduct__discontBefore"></div>
                  </div>
                  <img src="img/price_3.png" alt="">
                </div>
                <div class="sliderProduct__price">
                  <div class="slederPresent__priceNew">
                    569,00 ₽
                  </div>
                  <div class="slederPresent__priceOld">
                    669,00 ₽
                  </div>
                </div>
                <div class="slederPresent__lineDark"></div>
                <h4 class="greenColor">Блокнот Daisy, cо страницами для заметок</h4>
                <div class="slederPresent__color">
                  <div class="color__sircle">
                      <div class="color__sircleItem black"></div>
                      <div class="color__sircleItem color__sircleItemActive grey"></div>
                      <div class="color__sircleItem red"></div>
                      <div class="color__sircleItem brown"></div>
                      <div class="color__sircleItem green"></div>
                      <div class="color__sircleItem yellow"></div>
                      <a href="#" class="colorOpen"></a>
                  </div>
                </div>
                <div class="slederPresent__available">
                  В наличии – 13895 шт
                </div>
                <div class="slederPresent__view">
                  <a href="#" class="slederPresent__viewLink">
                    Быстрый просмотр
                  </a>
                  <a href="#" class="slederPresent__viewBasket">
                    <img src="img/basketIcon.svg" alt="Корзина">
                  </a>
                </div>
              </div>
            </div>
            <div class="sliderProduct__item ">
              <div class="sliderProduct">
                <div class="sliderProduct__image">
                  <div class="sliderProduct__discont">
                    <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                    <div class="sliderProduct__discontBefore"></div>
                  </div>
                  <img src="img/price_4.png" alt="">
                </div>
                <div class="sliderProduct__price">
                  <div class="slederPresent__priceNew">
                    569,00 ₽
                  </div>
                  <div class="slederPresent__priceOld">
                    669,00 ₽
                  </div>
                </div>
                <div class="slederPresent__lineDark"></div>
                <h4>Блокнот Daisy, cо страницами для заметок</h4>
                <div class="slederPresent__color"></div>
                <div class="slederPresent__available">
                  В наличии – 13895 шт
                </div>
                <div class="slederPresent__view">
                  <a href="#" class="slederPresent__viewLink">
                    Быстрый просмотр
                  </a>
                  <a href="#" class="slederPresent__viewBasket">
                    <img src="img/basketIcon.svg" alt="Корзина">
                  </a>
                </div>
              </div>
            </div>
            <div class="sliderProduct__item ">
              <div class="sliderProduct">
                <div class="sliderProduct__image">
                  <div class="sliderProduct__discont">
                    <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                    <div class="sliderProduct__discontBefore"></div>
                  </div>
                  <img src="img/price_5.png" alt="">
                </div>
                <div class="sliderProduct__price">
                  <div class="slederPresent__priceNew">
                    569,00 ₽
                  </div>
                  <div class="slederPresent__priceOld">
                    669,00 ₽
                  </div>
                </div>
                <div class="slederPresent__lineDark"></div>
                <h4>Блокнот Daisy, cо страницами для заметок</h4>
                <div class="slederPresent__color">
                  <div class="color__sircle">
                      <div class="color__sircleItem color__sircleItemActive blue"></div>
                      <div class="color__sircleItem greenLight"></div>
                      <div class="color__sircleItem red"></div>
                      <div class="color__sircleItem FFDA16"></div>
                  </div>
                </div>
                <div class="slederPresent__available">
                  В наличии – 13895 шт
                </div>
                <div class="slederPresent__view">
                  <a href="#" class="slederPresent__viewLink">
                    Быстрый просмотр
                  </a>
                  <a href="#" class="slederPresent__viewBasket">
                    <img src="img/basketIcon.svg" alt="Корзина">
                  </a>
                </div>
              </div>
            </div>
            <div class="sliderProduct__item ">
              <div class="sliderProduct">
                <div class="sliderProduct__image">
                  <div class="sliderProduct__discont">
                    <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                    <div class="sliderProduct__discontBefore"></div>
                  </div>
                  <img src="img/price_6.png" alt="">
                </div>
                <div class="sliderProduct__price">
                  <div class="slederPresent__priceNew">
                    569,00 ₽
                  </div>
                  <div class="slederPresent__priceOld">
                    669,00 ₽
                  </div>
                </div>
                <div class="slederPresent__lineDark"></div>
                <h4>Блокнот Daisy, cо страницами для заметок</h4>
                <div class="slederPresent__color"></div>
                <div class="slederPresent__available">
                  В наличии – 13895 шт
                </div>
                <div class="slederPresent__view">
                  <a href="#" class="slederPresent__viewLink">
                    Быстрый просмотр
                  </a>
                  <a href="#" class="slederPresent__viewBasket">
                    <img src="img/basketIcon.svg" alt="Корзина">
                  </a>
                </div>
              </div>
            </div>
            <div class="sliderProduct__item ">
              <div class="sliderProduct">
                <div class="sliderProduct__image">
                  <div class="sliderProduct__discont">
                    <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                    <div class="sliderProduct__discontBefore"></div>
                  </div>
                  <img src="img/price_1.png" alt="">
                </div>
                <div class="sliderProduct__price">
                  <div class="slederPresent__priceNew">
                    569,00 ₽
                  </div>
                  <div class="slederPresent__priceOld">
                    669,00 ₽
                  </div>
                </div>
                <div class="slederPresent__lineDark"></div>
                <h4>Блокнот Daisy, cо страницами для заметок</h4>
                <div class="slederPresent__color">
                  <div class="color__sircle">
                      <div class="color__sircleItem black"></div>
                      <div class="color__sircleItem color__sircleItemActive grey"></div>
                      <div class="color__sircleItem red"></div>
                      <div class="color__sircleItem brown"></div>
                      <div class="color__sircleItem green"></div>
                      <div class="color__sircleItem gold"></div>
                      <a href="#" class="colorOpen"></a>
                  </div>
                </div>
                <div class="slederPresent__available">
                  В наличии – 13895 шт
                </div>
                <div class="slederPresent__view">
                  <a href="#" class="slederPresent__viewLink">
                    Быстрый просмотр
                  </a>
                  <a href="#" class="slederPresent__viewBasket">
                    <img src="img/basketIcon.svg" alt="Корзина">
                  </a>
                </div>
              </div>
            </div>
            <div class="sliderProduct__item ">
              <div class="sliderProduct">
                <div class="sliderProduct__image">
                  <div class="sliderProduct__discont">
                    <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                    <div class="sliderProduct__discontBefore"></div>
                  </div>
                  <img src="img/price_2.png" alt="">
                </div>
                <div class="sliderProduct__price">
                  <div class="slederPresent__priceNew">
                    569,00 ₽
                  </div>
                  <div class="slederPresent__priceOld">
                    669,00 ₽
                  </div>
                </div>
                <div class="slederPresent__lineDark"></div>
                <h4>Блокнот Daisy, cо страницами для заметок</h4>
                <div class="slederPresent__color"></div>
                <div class="slederPresent__available">
                  В наличии – 13895 шт
                </div>
                <div class="slederPresent__view">
                  <a href="#" class="slederPresent__viewLink">
                    Быстрый просмотр
                  </a>
                  <a href="#" class="slederPresent__viewBasket">
                    <img src="img/basketIcon.svg" alt="Корзина">
                  </a>
                </div>
              </div>
            </div>
            <div class="sliderProduct__item ">
              <div class="sliderProduct">
                <div class="sliderProduct__image">
                  <div class="sliderProduct__discont">
                    <div class="sliderProduct__discontAfter rowCenter-xs">-21%</div>
                    <div class="sliderProduct__discontBefore"></div>
                  </div>
                  <img src="img/price_3.png" alt="">
                </div>
                <div class="sliderProduct__price">
                  <div class="slederPresent__priceNew">
                    569,00 ₽
                  </div>
                  <div class="slederPresent__priceOld">
                    669,00 ₽
                  </div>
                </div>
                <div class="slederPresent__lineDark"></div>
                <h4 class="greenColor">Блокнот Daisy, cо страницами для заметок</h4>
                <div class="slederPresent__color">
                  <div class="color__sircle">
                      <div class="color__sircleItem black"></div>
                      <div class="color__sircleItem color__sircleItemActive grey"></div>
                      <div class="color__sircleItem red"></div>
                      <div class="color__sircleItem brown"></div>
                      <div class="color__sircleItem green"></div>
                      <div class="color__sircleItem yellow"></div>
                      <a href="#" class="colorOpen"></a>
                  </div>
                </div>
                <div class="slederPresent__available">
                  В наличии – 13895 шт
                </div>
                <div class="slederPresent__view">
                  <a href="#" class="slederPresent__viewLink">
                    Быстрый просмотр
                  </a>
                  <a href="#" class="slederPresent__viewBasket">
                    <img src="img/basketIcon.svg" alt="Корзина">
                  </a>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="col-xs-12">
        <div class="openLink price__openLink middle-xs">
            <a href="#" class="plus rowCenter-xs">+</a>
            <a href="#" class="text">больше товаров со скидками</a>
          </div>
      </div>
    </div>
  </div>
  <div class="newItems">
    <div class="container-fluid container">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="line"></div>
        </div>
        <div class="col-xs-12">
          <h2>новинки</h2>
        </div>
      
        <div class="col-xs-12 column-xs">
          <div class="sliderNewProdact sliderAll__wrapper">
              <div class="sliderProduct__item">
                <div class="sliderProduct">
                  <div class="sliderProduct__image">
                    <div class="sliderProduct__discont sliderProduct__discont">
                      <div class="sliderProduct__discontAfter sliderProduct__newAfter rowCenter-xs">new</div>
                      <div class="sliderProduct__discontBefore"></div>
                    </div>
                    <img src="img/price_1.png" alt="">
                  </div>
                  <div class="sliderProduct__price">
                    <div class="slederPresent__priceBlack">
                      569,00 ₽
                    </div>
                  </div>
                  <div class="slederPresent__lineDark"></div>
                  <h4>Блокнот Daisy, cо страницами для заметок</h4>
                  <div class="slederPresent__color">
                    <div class="color__sircle"></div>
                  </div>
                  <div class="slederPresent__available">
                    В наличии – 13895 шт
                  </div>
                  <div class="slederPresent__view">
                    <a href="#" class="slederPresent__viewLink">
                      Быстрый просмотр
                    </a>
                    <a href="#" class="slederPresent__viewBasket">
                      <img src="img/basketIcon.svg" alt="Корзина">
                    </a>
                  </div>
                </div>
              </div>
              <div class="sliderProduct__item">
                <div class="sliderProduct">
                  <div class="sliderProduct__image">
                    <div class="sliderProduct__discont sliderProduct__discont">
                      <div class="sliderProduct__discontAfter sliderProduct__newAfter rowCenter-xs">new</div>
                      <div class="sliderProduct__discontBefore"></div>
                    </div>
                    <img src="img/price_2.png" alt="">
                  </div>
                  <div class="sliderProduct__price">
                    <div class="slederPresent__priceNew">
                      569,00 ₽
                    </div>
                    <div class="slederPresent__priceOld">
                      669,00 ₽
                    </div>
                  </div>
                  <div class="slederPresent__lineDark"></div>
                  <h4>Блокнот Daisy, cо страницами для заметок</h4>
                  <div class="slederPresent__color"></div>
                  <div class="slederPresent__available">
                    В наличии – 13895 шт
                  </div>
                  <div class="slederPresent__view">
                    <a href="#" class="slederPresent__viewLink">
                      Быстрый просмотр
                    </a>
                    <a href="#" class="slederPresent__viewBasket">
                      <img src="img/basketIcon.svg" alt="Корзина">
                    </a>
                  </div>
                </div>
              </div>
              <div class="sliderProduct__item">
                <div class="sliderProduct">
                  <div class="sliderProduct__image">
                    <div class="sliderProduct__discont sliderProduct__discont">
                      <div class="sliderProduct__discontAfter sliderProduct__newAfter rowCenter-xs">new</div>
                      <div class="sliderProduct__discontBefore"></div>
                    </div>
                    <img src="img/price_3.png" alt="">
                  </div>
                  <div class="sliderProduct__price">
                    <div class="slederPresent__priceBlack">
                      569,00 ₽
                    </div>
                  </div>
                  <div class="slederPresent__lineDark"></div>
                  <h4 class="greenColor">Блокнот Daisy, cо страницами для заметок</h4>
                  <div class="slederPresent__color">
                    <div class="color__sircle"></div>
                  </div>
                  <div class="slederPresent__available">
                    В наличии – 13895 шт
                  </div>
                  <div class="slederPresent__view">
                    <a href="#" class="slederPresent__viewLink">
                      Быстрый просмотр
                    </a>
                    <a href="#" class="slederPresent__viewBasket">
                      <img src="img/basketIcon.svg" alt="Корзина">
                    </a>
                  </div>
                </div>
              </div>
              <div class="sliderProduct__item">
                <div class="sliderProduct">
                  <div class="sliderProduct__image">
                    <div class="sliderProduct__discont sliderProduct__discont">
                      <div class="sliderProduct__discontAfter sliderProduct__newAfter rowCenter-xs">new</div>
                      <div class="sliderProduct__discontBefore"></div>
                    </div>
                    <img src="img/price_4.png" alt="">
                  </div>
                  <div class="sliderProduct__price">
                    <div class="slederPresent__priceBlack">
                      569,00 ₽
                    </div>
                  </div>
                  <div class="slederPresent__lineDark"></div>
                  <h4>Блокнот Daisy, cо страницами для заметок</h4>
                  <div class="slederPresent__color"></div>
                  <div class="slederPresent__available">
                    В наличии – 13895 шт
                  </div>
                  <div class="slederPresent__view">
                    <a href="#" class="slederPresent__viewLink">
                      Быстрый просмотр
                    </a>
                    <a href="#" class="slederPresent__viewBasket">
                      <img src="img/basketIcon.svg" alt="Корзина">
                    </a>
                  </div>
                </div>
              </div>
              <div class="sliderProduct__item">
                <div class="sliderProduct">
                  <div class="sliderProduct__image">
                    <div class="sliderProduct__discont sliderProduct__discont">
                      <div class="sliderProduct__discontAfter sliderProduct__newAfter rowCenter-xs">new</div>
                      <div class="sliderProduct__discontBefore"></div>
                    </div>
                    <img src="img/price_5.png" alt="">
                  </div>
                  <div class="sliderProduct__price">
                    <div class="slederPresent__priceBlack">
                      569,00 ₽
                    </div>
                  </div>
                  <div class="slederPresent__lineDark"></div>
                  <h4>Блокнот Daisy, cо страницами для заметок</h4>
                  <div class="slederPresent__color">
                    <div class="color__sircle"></div>
                  </div>
                  <div class="slederPresent__available">
                    В наличии – 13895 шт
                  </div>
                  <div class="slederPresent__view">
                    <a href="#" class="slederPresent__viewLink">
                      Быстрый просмотр
                    </a>
                    <a href="#" class="slederPresent__viewBasket">
                      <img src="img/basketIcon.svg" alt="Корзина">
                    </a>
                  </div>
                </div>
              </div>
              <div class="sliderProduct__item">
                <div class="sliderProduct">
                  <div class="sliderProduct__image">
                    <div class="sliderProduct__discont sliderProduct__discont">
                      <div class="sliderProduct__discontAfter sliderProduct__newAfter rowCenter-xs">new</div>
                      <div class="sliderProduct__discontBefore"></div>
                    </div>
                    <img src="img/price_6.png" alt="">
                  </div>
                  <div class="sliderProduct__price">
                    <div class="slederPresent__priceNew">
                      569,00 ₽
                    </div>
                    <div class="slederPresent__priceOld">
                      669,00 ₽
                    </div>
                  </div>
                  <div class="slederPresent__lineDark"></div>
                  <h4>Блокнот Daisy, cо страницами для заметок</h4>
                  <div class="slederPresent__color"></div>
                  <div class="slederPresent__available">
                    В наличии – 13895 шт
                  </div>
                  <div class="slederPresent__view">
                    <a href="#" class="slederPresent__viewLink">
                      Быстрый просмотр
                    </a>
                    <a href="#" class="slederPresent__viewBasket">
                      <img src="img/basketIcon.svg" alt="Корзина">
                    </a>
                  </div>
                </div>
              </div>
              <div class="sliderProduct__item">
                <div class="sliderProduct">
                  <div class="sliderProduct__image">
                    <div class="sliderProduct__discont sliderProduct__discont">
                      <div class="sliderProduct__discontAfter sliderProduct__newAfter rowCenter-xs">new</div>
                      <div class="sliderProduct__discontBefore"></div>
                    </div>
                    <img src="img/price_1.png" alt="">
                  </div>
                  <div class="sliderProduct__price">
                    <div class="slederPresent__priceBlack">
                      569,00 ₽
                    </div>
                  </div>
                  <div class="slederPresent__lineDark"></div>
                  <h4>Блокнот Daisy, cо страницами для заметок</h4>
                  <div class="slederPresent__color">
                    <div class="color__sircle"></div>
                  </div>
                  <div class="slederPresent__available">
                    В наличии – 13895 шт
                  </div>
                  <div class="slederPresent__view">
                    <a href="#" class="slederPresent__viewLink">
                      Быстрый просмотр
                    </a>
                    <a href="#" class="slederPresent__viewBasket">
                      <img src="img/basketIcon.svg" alt="Корзина">
                    </a>
                  </div>
                </div>
              </div>
              <div class="sliderProduct__item">
                <div class="sliderProduct">
                  <div class="sliderProduct__image">
                    <div class="sliderProduct__discont sliderProduct__discont">
                      <div class="sliderProduct__discontAfter sliderProduct__newAfter rowCenter-xs">new</div>
                      <div class="sliderProduct__discontBefore"></div>
                    </div>
                    <img src="img/price_2.png" alt="">
                  </div>
                  <div class="sliderProduct__price">
                    <div class="slederPresent__priceBlack">
                      569,00 ₽
                    </div>
                  </div>
                  <div class="slederPresent__lineDark"></div>
                  <h4>Блокнот Daisy, cо страницами для заметок</h4>
                  <div class="slederPresent__color"></div>
                  <div class="slederPresent__available">
                    В наличии – 13895 шт
                  </div>
                  <div class="slederPresent__view">
                    <a href="#" class="slederPresent__viewLink">
                      Быстрый просмотр
                    </a>
                    <a href="#" class="slederPresent__viewBasket">
                      <img src="img/basketIcon.svg" alt="Корзина">
                    </a>
                  </div>
                </div>
              </div>
              <div class="sliderProduct__item">
                <div class="sliderProduct">
                  <div class="sliderProduct__image">
                    <div class="sliderProduct__discont sliderProduct__discont">
                      <div class="sliderProduct__discontAfter sliderProduct__newAfter rowCenter-xs">new</div>
                      <div class="sliderProduct__discontBefore"></div>
                    </div>
                    <img src="img/price_3.png" alt="">
                  </div>
                  <div class="sliderProduct__price">
                    <div class="slederPresent__priceBlack">
                      569,00 ₽
                    </div>
                  </div>
                  <div class="slederPresent__lineDark"></div>
                  <h4 class="greenColor">Блокнот Daisy, cо страницами для заметок</h4>
                  <div class="slederPresent__color">
                    <div class="color__sircle"></div>
                  </div>
                  <div class="slederPresent__available">
                    В наличии – 13895 шт
                  </div>
                  <div class="slederPresent__view">
                    <a href="#" class="slederPresent__viewLink">
                      Быстрый просмотр
                    </a>
                    <a href="#" class="slederPresent__viewBasket">
                      <img src="img/basketIcon.svg" alt="Корзина">
                    </a>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="openLink middle-xs">
            <a href="#" class="plus rowCenter-xs">+</a>
            <a href="#" class="text">больше новинок</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="company">
    <div class="container-fluid container">
      <div class="row">
        <div class="col-xs-12">
          <div class="line"></div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-xs-12">
          <h2>сувенир.рф выбирают</h2>
        </div>
      </div>
    </div>
    <div class="sliderCompany sliderAll__wrapper">
      <div class="sliderCompany__item">
        <img src="img/company_1.svg" alt="Циан">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_2.svg" alt="kaspersky">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_3.svg" alt="netflix">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_4.svg" alt="газпром">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_5.svg" alt="яндекс">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_6.svg" alt="google">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_1.svg" alt="Циан">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_2.svg" alt="kaspersky">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_3.svg" alt="netflix">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_4.svg" alt="газпром">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_5.svg" alt="яндекс">
      </div>
      <div class="sliderCompany__item">
        <img src="img/company_6.svg" alt="google">
      </div>
    </div>
  </div>
  <div class="statistic">
    <div class="container-fluid container">
      <div class="row">
        <div class="col-xs-12">
          <div class="line"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 column-smm">
          <div class="statistic__text">
            <p>
            Cайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия. По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских
            </p>
            <a href="#">читать ещё
              <img src="img/arrGreen.svg" alt="читать ещё">
            </a>
          </div>
          <div class="statistic__num">
            <div class="statistic__numText">
              <p class="statistic__big">19204</p>
              <p class="statistic__small">сувениров на сайте</p>
              <p class="statistic__big">236</p>
              <p class="statistic__small">напечатанных логотипов</p>
              <p class="statistic__big">1435</p>
              <p class="statistic__small">Выполненных заказов</p>
            </div>
             
          </div>
        </div>
        <div class="col-xs-12">
          <div class="line statistic__line"></div>
        </div>
      </div>
    </div>
    <?php include 'footerContainer.php' ?>
  </div>
  
  <?php include 'footer.php' ?>


  <script src='js/script.js'></script>
</body>
</html>